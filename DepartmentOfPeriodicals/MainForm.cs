﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DepartmentOfPeriodicals.View;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;

namespace DepartmentOfPeriodicals
{
    public partial class MainForm : Form
    {
        private DepartmentOfPeriodicalsContext _context;
        private bool IsSupply;
        private SupplyForm form;

        public MainForm()
        {
            InitializeComponent();
            _context = new DepartmentOfPeriodicalsContext();
            IsSupply = false;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "supplyDataSet.Supply". При необходимости она может быть перемещена или удалена.
            this.supplyTableAdapter.Fill(this.supplyDataSet.Supply);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "supplyDataSet.Supply". При необходимости она может быть перемещена или удалена.
            this.supplyTableAdapter.Fill(this.supplyDataSet.Supply);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "subjectsDataSet.Publication". При необходимости она может быть перемещена или удалена.
            this.publicationTableAdapter2.Fill(this.subjectsDataSet.Publication);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "typeDataSet.Publication". При необходимости она может быть перемещена или удалена.
            this.publicationTableAdapter1.Fill(this.typeDataSet.Publication);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "publicationDataSet.Publication". При необходимости она может быть перемещена или удалена.
            this.publicationTableAdapter.Fill(this.publicationDataSet.Publication);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "publishingHouseDataSet.PublishingHouse". При необходимости она может быть перемещена или удалена.
            this.publishingHouseTableAdapter.Fill(this.publishingHouseDataSet.PublishingHouse);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "readerDataSet.Reader". При необходимости она может быть перемещена или удалена.
            this.readerTableAdapter.Fill(this.readerDataSet.Reader);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "issueDataSet.Issue". При необходимости она может быть перемещена или удалена.
            this.issueTableAdapter.Fill(this.issueDataSet.Issue);
}

        private void buttonReaderSearch_Click(object sender, EventArgs e)
        {
            List<Reader> readers = new List<Reader>();
            if (textBoxReaderName.Text != String.Empty)
            {
                readers = _context.Reader.Where(s => s.Name.ToLower().Contains(textBoxReaderName.Text.ToLower())).ToList();
            }
            if (textBoxReaderSurname.Text != String.Empty)
            {
                foreach(var reader in _context.Reader.Where(s => s.Surname.ToLower().Contains(textBoxReaderSurname.Text.ToLower())).ToList())
                {
                    readers.Add(reader);
                }
            }
            else if ((int)numericUpDownReaderNumber.Value != 0)
            {
                readers = _context.Reader.Where(s => s.ReaderNumber == (int)numericUpDownReaderNumber.Value).ToList();
            }
            dataGridViewReader.DataSource = readers;
        }

        private void buttonReaderReset_Click(object sender, EventArgs e)
        {
            textBoxReaderName.Text = String.Empty;
            textBoxReaderSurname.Text = String.Empty;
            numericUpDownReaderNumber.Value = 0;
            dataGridViewReader.DataSource = readerDataSet.Reader;
        }

        private void buttonIssueSearch_Click(object sender, EventArgs e)
        {
            string filter = String.Empty;
            if (numericUpDownIssueISSN.Value != 0)
            {
                filter = string.Format("ISSN = '{0}'", numericUpDownIssueISSN.Value.ToString());
            }
            else if (textBoxIssueName.Text != String.Empty)
            {
                filter = string.Format("Name = '{0}'", textBoxIssueName.Text);
            }
            if (numericUpDownIssueNumber.Value != 0)
            {
                filter = CombineCriteria(filter, string.Format("IssueNumber = '{0}'", numericUpDownIssueNumber.Value.ToString()));
            }
            issueBindingSource.Filter = filter;
        }

        private void buttonIssueClear_Click(object sender, EventArgs e)
        {
            numericUpDownIssueISSN.Value = 0;
            textBoxIssueName.Text = String.Empty;
            numericUpDownIssueNumber.Value = 0;
            issueBindingSource.RemoveFilter();
        }

        private void buttonPublicationSearch_Click(object sender, EventArgs e)
        {
            string filter = String.Empty;
            if (comboBoxType.SelectedIndex != -1)
            {
                filter = string.Format(
                    "Type = '{0}'", comboBoxType.SelectedValue.ToString());
            }
            if (comboBoxSubject.SelectedIndex != -1)
            {
                filter = CombineCriteria(filter, string.Format(
                    "Subjects = '{0}'", comboBoxSubject.SelectedValue.ToString()));
            }
            filter = CombineCriteria(filter, string.Format(
                "Periodicity <= {0}", 
                numericUpDownLess.Value.ToString()));
            filter = CombineCriteria(filter, string.Format(
                "Periodicity >= {0}",
                numericUpDownMore.Value.ToString()));
            publicationBindingSource.Filter = filter;
        }

        private void buttonPubicationReset_Click(object sender, EventArgs e)
        {
            numericUpDownMore.Value = 1;
            numericUpDownLess.Value = 7;
            comboBoxType.SelectedIndex = -1;
            comboBoxSubject.SelectedIndex = -1;
            publicationBindingSource.RemoveFilter();
        }

        private void buttonSupplySearch_Click(object sender, EventArgs e)
        {
            string filter = String.Empty;
            if (Int32.Parse(numericUpDown1.Value.ToString()) != 0)
            {
                filter = String.Format(
                    "ReaderNumber = '{0}'", numericUpDown1.Value.ToString());
            }
            supplyBindingSource.Filter = filter;
        }

        private void buttonSupplyClear_Click(object sender, EventArgs e)
        {
            numericUpDown1.Value = 0;
            supplyBindingSource.RemoveFilter();
        }

        private static string CombineCriteria(string oldCondition, string newCondition)
        {
            if (string.IsNullOrEmpty(oldCondition))
                return newCondition;

            return "(" + oldCondition + ") AND (" + newCondition + ")";
        }

        /// <summary>
        /// Добавить читателя
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void читателяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReaderForm form = new ReaderForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                Reader temp = new Reader();
                temp.Name = form.ReaderName;
                temp.Surname = form.Surname;
                temp.Phone = form.Phone;
                temp.DOB = form.DOB;
                temp.Address = form.Address;

                _context.Reader.Add(temp);
                _context.SaveChanges();
                this.readerTableAdapter.Fill(
                    this.readerDataSet.Reader);
            }
        }

        /// <summary>
        /// Добавить издательство
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void издательствоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PublishingHouseForm form = new PublishingHouseForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                PublishingHouse temp = new PublishingHouse();
                temp.Name = form.PublishingHouseName;
                temp.Phone = form.Phone;
                temp.Address = form.Address;

                _context.PublishingHouse.Add(temp);
                _context.SaveChanges();
                this.publishingHouseTableAdapter.Fill(
                    this.publishingHouseDataSet.PublishingHouse);
            }
        }

        /// <summary>
        /// Добавить издание
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void изданиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PublicationForm form = new PublicationForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                Publication temp = new Publication();
                temp.ISSN = form.ISSN;
                temp.Name = form.publicationName;
                temp.Periodicity = form.periodicity;
                temp.PublishingHouseId = form.publishingHouseId;
                temp.Subjects = form.subjects;
                temp.Type = form.type;

                _context.Publication.Add(temp);
                _context.SaveChanges();
                this.publicationTableAdapter.Fill(
                    this.publicationDataSet.Publication);
                //Чтобы добавить значение subject и type
                this.publicationTableAdapter1.Fill(
                        this.typeDataSet.Publication);
                this.publicationTableAdapter2.Fill(
                    this.subjectsDataSet.Publication);
            }
        }

        /// <summary>
        /// Добавить выпуск
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void выпускToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IssueForm form = new IssueForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                Issue issue = new Issue();
                issue.Date = form.date;
                issue.ISSN = form.issueISSN;
                issue.IssueNumber = form.issueNumber;
                issue.Period = form.period;
                issue.Printing = form.printing;
                issue.Quantity = form.quantity;
                _context.Issue.Add(issue);
                _context.SaveChanges();
                this.issueTableAdapter.Fill(
                    this.issueDataSet.Issue);
            }
        }

        /// <summary>
        /// Контекст.меню изменить
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void изменитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            switch (tabControl.SelectedIndex)
            {
                case 0:
                    int readerId = Int32.Parse(
                        dataGridViewReader.SelectedRows[0].Cells[0].Value.ToString());
                    Reader reader = _context.Reader.First(s => s.ReaderNumber == readerId);
                    ReaderForm readerForm = new ReaderForm(reader);
                    if (readerForm.ShowDialog() == DialogResult.OK)
                    {
                        reader.Name = readerForm.ReaderName;
                        reader.Phone = readerForm.Phone;
                        reader.Surname = readerForm.Surname;
                        reader.DOB = readerForm.DOB;
                        reader.Address = readerForm.Address;
                        _context.SaveChanges();
                        this.readerTableAdapter.Fill(
                            this.readerDataSet.Reader);
                    }
                    return;
                case 1:
                    int houseId = Int32.Parse(
                        dataGridViewPublishingHouse.SelectedRows[0].Cells[0].Value.ToString());
                    PublishingHouse house = _context.PublishingHouse.First(
                        s => s.Id == houseId);
                    PublishingHouseForm houseForm = new PublishingHouseForm(house);
                    if (houseForm.ShowDialog() == DialogResult.OK)
                    {
                        house.Name = houseForm.PublishingHouseName;
                        house.Phone = houseForm.Phone;
                        house.Address = houseForm.Address;
                        _context.SaveChanges();
                        this.publishingHouseTableAdapter.Fill(
                            this.publishingHouseDataSet.PublishingHouse);
                        //Чтобы изменились данные у изданий
                        this.publicationTableAdapter.Fill(
                            this.publicationDataSet.Publication);
                    }
                    return;
                case 2:
                    int publicationISSN = Int32.Parse(
                        dataGridViewPublication.SelectedRows[0].Cells[0].Value.ToString());
                    Publication publication = _context.Publication.First(
                        s => s.ISSN == publicationISSN);
                    PublicationForm publicationForm = new PublicationForm(publication);
                    if (publicationForm.ShowDialog() == DialogResult.OK)
                    {
                        publication.Name = publicationForm.publicationName;
                        publication.Type = publicationForm.type;
                        publication.Subjects = publicationForm.subjects;
                        publication.Periodicity = publicationForm.periodicity;
                        publication.PublishingHouseId = publicationForm.publishingHouseId;
                        _context.SaveChanges();
                        this.publicationTableAdapter.Fill(
                            this.publicationDataSet.Publication);
                        //Чтобы изменились данные у выпусков
                        this.issueTableAdapter.Fill(
                            this.issueDataSet.Issue);
                    }
                    this.publicationTableAdapter1.Fill(
                        this.typeDataSet.Publication);
                    this.publicationTableAdapter2.Fill(
                        this.subjectsDataSet.Publication);
                    return;
                case 3:
                    int issueId = Int32.Parse(
                        dataGridViewIssue.SelectedRows[0].Cells[0].Value.ToString());
                    Issue issue = _context.Issue.First(
                        s => s.Id == issueId);
                    IssueForm issueForm = new IssueForm(issue);
                    if (issueForm.ShowDialog() == DialogResult.OK)
                    {
                        issue.IssueNumber = issueForm.issueNumber;
                        issue.ISSN = issueForm.issueISSN;
                        issue.Date = issueForm.date;
                        issue.Period = issueForm.period;
                        issue.Printing = issueForm.printing;
                        issue.Quantity = issueForm.quantity;
                        _context.SaveChanges();
                        this.issueTableAdapter.Fill(
                            this.issueDataSet.Issue);
                    }
                    return;
            }

        }

        /// <summary>
        /// Контекст.меню удалить
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void удалитьToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            switch (tabControl.SelectedIndex)
            {
                case 0:
                    int readerId = Int32.Parse(
                        dataGridViewReader.SelectedRows[0].Cells[0].Value.ToString());
                    Reader reader = _context.Reader.First(s => s.ReaderNumber == readerId);
                    if (MessageBox.Show("Вы подтверждаете удаление?", "Внимание!",
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        _context.Reader.Remove(reader);
                        _context.SaveChanges();
                        this.readerTableAdapter.Fill(
                            this.readerDataSet.Reader);
                    }
                    return;
                case 1:
                    int houseId = Int32.Parse(
                        dataGridViewPublishingHouse.SelectedRows[0].Cells[0].Value.ToString());
                    PublishingHouse house = _context.PublishingHouse.First(s => s.Id == houseId);
                    house.Publications = _context.Publication.Where(
                        s => s.PublishingHouseId == house.Id).ToList();
                    if (MessageBox.Show("Вы подтверждаете удаление?", "Внимание!",
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        _context.PublishingHouse.Remove(house);
                        _context.SaveChanges();
                        this.publishingHouseTableAdapter.Fill(
                            this.publishingHouseDataSet.PublishingHouse);
                        //Чтобы изменились данные у изданий
                        this.publicationTableAdapter.Fill(
                            this.publicationDataSet.Publication);
                    }
                    return;
                case 2:
                    int publicationISSN = Int32.Parse(
                        dataGridViewPublication.SelectedRows[0].Cells[0].Value.ToString());
                    Publication publication = _context.Publication.First(
                        s => s.ISSN == publicationISSN);
                    publication.Issues = _context.Issue.Where(
                        s => s.ISSN == publication.ISSN).ToList();
                    if (MessageBox.Show("Вы подтверждаете удаление?", "Внимание!",
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        _context.Publication.Remove(publication);
                        _context.SaveChanges();
                        this.publicationTableAdapter.Fill(
                            this.publicationDataSet.Publication);
                        //Чтобы изменились данные у выпусков
                        this.issueTableAdapter.Fill(
                            this.issueDataSet.Issue);
                    }
                    this.publicationTableAdapter1.Fill(
                        this.typeDataSet.Publication);
                    this.publicationTableAdapter2.Fill(
                        this.subjectsDataSet.Publication);
                    return;
                case 3:
                    int issueId = Int32.Parse(
                        dataGridViewIssue.SelectedRows[0].Cells[0].Value.ToString());
                    Issue issue = _context.Issue.First(
                        s => s.Id == issueId);
                    if (MessageBox.Show("Вы подтверждаете удаление?", "Внимание!",
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        _context.Issue.Remove(issue);
                        _context.SaveChanges();
                        this.issueTableAdapter.Fill(
                            this.issueDataSet.Issue);
                    }
                    return;
            }
        }

        /// <summary>
        /// Контекст.меню добавить в выдачу
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void добавитьВВыдачуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Issue adding = new Issue();
            int Id = Int32.Parse(dataGridViewIssue.SelectedRows[0].Cells[0].Value.ToString());
            adding = _context.Issue.First(s => s.Id == Id);
            if (adding.Quantity == 0)
            {
                MessageBox.Show("Этого выпуска нет в наличии!");
                return;
            }
            if (!IsSupply)
            {
                IsSupply = true;
                form = new SupplyForm();
                form.FormClosed += SupplyForm_Closed;
                form.Show();
                form.AddElement(adding);
            }
            else
            {
                form.AddElement(adding);
            }
        }

        /// <summary>
        /// Контекст.меню выдать периодику
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void выдатьПериодикуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int readerId = Int32.Parse(
                        dataGridViewReader.SelectedRows[0].Cells[0].Value.ToString());
            Reader reader = _context.Reader.First(s => s.ReaderNumber == readerId);
            if (reader.Fines >= 3)
            {
                MessageBox.Show("У этого читателя слишком много штрафов!", "Внимание!",
                    MessageBoxButtons.OK);
                return;
            }
            else
            {
                form.AddReader(reader);
            }
        }

        /// <summary>
        /// Контекст.меню распечатать
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void распечататьЧитательскийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int readerId = Int32.Parse(
                        dataGridViewReader.SelectedRows[0].Cells[0].Value.ToString());
            Reader reader = _context.Reader.First(s => s.ReaderNumber == readerId);

            string ttf = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIAL.TTF");
            var baseFont = BaseFont.CreateFont(ttf, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            var font = new iTextSharp.text.Font(baseFont, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL);

            using (var document = new Document())
            using (var stream = new FileStream(Application.StartupPath + @"\Lcard" + reader.ReaderNumber.ToString() + ".pdf", FileMode.Create))
            {
                document.SetPageSize(PageSize.A6.Rotate()); // landscape
                PdfWriter.GetInstance(document, stream);
                document.Open();

                PdfPTable table = new PdfPTable(3);
                BarcodeQRCode qrcode = new BarcodeQRCode(reader.ReaderNumber.ToString(), 1, 1, null);
                iTextSharp.text.Image img = qrcode.GetImage();
                PdfPCell cell = new PdfPCell(img, true);
                table.AddCell(cell);
                Phrase p = new Phrase();
                Chunk c1 = new Chunk("Читательский билет #" + reader.ReaderNumber.ToString(), font);
                cell = new PdfPCell(new Phrase("Читательский билет #" + reader.ReaderNumber.ToString() + "\n" + "\n" + reader.Name.ToString() + " " + reader.Surname.ToString() + "\n" + "\n" + reader.DOB.ToShortDateString(), font));
                cell.Colspan = 2;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthLeft = 0f;
                table.AddCell(cell);
                table.AddCell(cell);
                cell = new PdfPCell(new Paragraph(reader.Name.ToString() + " " + reader.Surname.ToString(), font));
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                document.Add(table);

                document.Close();
            }

            Process.Start(Application.StartupPath + @"\Lcard" + reader.ReaderNumber.ToString() + ".pdf");


            //var doc = new Document();
            //PdfWriter.GetInstance(doc, new FileStream(Application.StartupPath + @"\Lcard" + reader.ReaderNumber.ToString() + ".pdf", FileMode.Create));
            //doc.Open();
            //PdfPTable table = new PdfPTable(2);
            //iTextSharp.text.Image book = iTextSharp.text.Image.GetInstance(Application.StartupPath + @"\news.png");
            //PdfPCell cell = new PdfPCell(book);
            //table.AddCell(cell);
            //cell = new PdfPCell(new Paragraph("Читательский билет #" + reader.ReaderNumber.ToString()));
            //table.AddCell(cell);
            //BarcodeQRCode qrcode = new BarcodeQRCode("Moby Dick by Herman Melville", 100, 100, null);
            //iTextSharp.text.Image img = qrcode.GetImage();
            //doc.Add(img);


            //PdfPTable table = new PdfPTable(3);
            //PdfPCell cell = new PdfPCell(new Phrase("Simple table",
            //  new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16,
            //  iTextSharp.text.Font.NORMAL, new BaseColor(Color.Orange))));
            //cell.BackgroundColor = new BaseColor(Color.Wheat);
            //cell.Padding = 5;
            //cell.Colspan = 3;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //table.AddCell(cell);
            //table.AddCell("Col 1 Row 1");
            //table.AddCell("Col 2 Row 1");
            //table.AddCell("Col 3 Row 1");
            //table.AddCell("Col 1 Row 2");
            //table.AddCell("Col 2 Row 2");
            //table.AddCell("Col 3 Row 2");
            //cell = new PdfPCell(new Phrase("Col 2 Row 3"));
            //cell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
            //cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            //table.AddCell(cell);
            //doc.Add(table);
            //doc.Close();
            //System.Diagnostics.Process.Start(Application.StartupPath + @"\Lcard" + reader.ReaderNumber.ToString() + ".pdf");
        }

        /// <summary>
        /// Добавление элементов в контекстное меню
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void contextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            if (tabControl.SelectedIndex == 0 && IsSupply == true)
            {
                HideAll();
                распечататьЧитательскийToolStripMenuItem.Visible = true;
                выдатьПериодикуToolStripMenuItem.Visible = true;
                изменитьToolStripMenuItem.Visible = true;
                удалитьToolStripMenuItem1.Visible = true;
            }
            else if (tabControl.SelectedIndex == 0)
            {
                HideAll();
                распечататьЧитательскийToolStripMenuItem.Visible = true;
                изменитьToolStripMenuItem.Visible = true;
                удалитьToolStripMenuItem1.Visible = true;
            }
            if (tabControl.SelectedIndex == 1)
            {
                HideAll();
                изменитьToolStripMenuItem.Visible = true;
                удалитьToolStripMenuItem1.Visible = true;
            }
            if (tabControl.SelectedIndex == 2)
            {
                HideAll();
                изменитьToolStripMenuItem.Visible = true;
                удалитьToolStripMenuItem1.Visible = true;
            }
            if (tabControl.SelectedIndex == 3)
            {
                HideAll();
                добавитьВВыдачуToolStripMenuItem.Visible = true;
                изменитьToolStripMenuItem.Visible = true;
                удалитьToolStripMenuItem1.Visible = true;
            }
            
            if (tabControl.SelectedIndex == 4)
            {
                HideAll();
                закрытьToolStripMenuItem.Visible = true;
            }
        }

        /// <summary>
        /// Прячем все пункты меню
        /// </summary>
        private void HideAll()
        {
            foreach (var item in contextMenuStrip.Items)
            {
                (item as ToolStripMenuItem).Visible = false;
            }
        }

        /// <summary>
        /// Выделение по правому клику
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewReader_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridViewReader.HitTest(e.X, e.Y);
                dataGridViewReader.ClearSelection();
                dataGridViewReader.Rows[hti.RowIndex].Selected = true;
            }
        }

        /// <summary>
        /// Выделение по правому клику
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPublishingHouse_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridViewPublishingHouse.HitTest(e.X, e.Y);
                dataGridViewPublishingHouse.ClearSelection();
                dataGridViewPublishingHouse.Rows[hti.RowIndex].Selected = true;
            }
        }

        /// <summary>
        /// Выделение по правому клику
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPublication_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridViewPublication.HitTest(e.X, e.Y);
                dataGridViewPublication.ClearSelection();
                dataGridViewPublication.Rows[hti.RowIndex].Selected = true;
            }
        }

        /// <summary>
        /// Выделение по правому клику
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewIssue_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridViewIssue.HitTest(e.X, e.Y);
                dataGridViewIssue.ClearSelection();
                dataGridViewIssue.Rows[hti.RowIndex].Selected = true;
            }
        }

        /// <summary>
        /// Отслеживаем закрытие формы и добавляем выдачу
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SupplyForm_Closed(object sender, FormClosedEventArgs e)
        {
            IsSupply = false;
            if ((sender as SupplyForm).DialogResult == DialogResult.OK)
            {
                Supply temp = new Supply();
                temp.ReaderNumber = (sender as SupplyForm).reader.ReaderNumber;
                temp.Date = DateTime.Now;
                temp.Return = false;
                foreach (var issue in (sender as SupplyForm).supply)
                {
                    temp.Entrances.Add(new Entrance() { IssueId = issue.Id });
                    _context.Issue.First(s => s.Id == issue.Id).Quantity--;
                }
                _context.Supply.Add(temp);
                _context.SaveChanges();
                this.supplyTableAdapter.Fill(
                    supplyDataSet.Supply);
            }
        }

        /// <summary>
        /// Закрываем выдачу
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            CloseSupply();
        }

        /// <summary>
        /// Закрываем выдачу
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void закрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseSupply();
        }

        /// <summary>
        /// Сам метод закрытия выдачи
        /// </summary>
        private void CloseSupply()
        {
            int Id = Int32.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            Supply supply = _context.Supply.First(s => s.Id == Id);
            form = new SupplyForm(supply, _context);
            if (form.ShowDialog() == DialogResult.OK)
            {
                foreach (var item in supply.Entrances)
                {
                    _context.Issue.First(s => s.Id == item.IssueId).Quantity++;
                }
                if (DateTime.Now.Date > supply.Date.AddDays(3).Date)
                {
                    _context.Reader.First(s => s.ReaderNumber == supply.ReaderNumber).Fines++;
                }
                supply.Return = true;
                _context.SaveChanges();
                this.supplyTableAdapter.Fill(
                    supplyDataSet.Supply);
                this.issueTableAdapter.Fill(
                    issueDataSet.Issue);
            }
        }

        private void должникиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Debtors form = new Debtors(_context);
            form.Show();
        }

        private void отсутствующиеВыпускиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrequentRequests form = new FrequentRequests();
            form.Show();
        }

        private void свежееПоступлениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewIssies form = new NewIssies(_context);
            form.Show();
        }

        private void произвольныйЗапросToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SQL form = new SQL();
            form.Show();
        }

        /// <summary>
        /// Отчет "Должники"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void должникиToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            List<Reader> list = new List<Reader>();
            foreach (var supply in _context.Supply.Where(s => s.Return == false))
            {
                list.Add(_context.Reader.First(s => s.ReaderNumber == supply.ReaderNumber));
            }
            string ttf = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIAL.TTF");
            var baseFont = BaseFont.CreateFont(ttf, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            var font = new iTextSharp.text.Font(baseFont, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL);

            using (var document = new Document())
            using (var stream = new FileStream(Application.StartupPath + @"\Debtors.pdf", FileMode.Create))
            {
                document.SetPageSize(PageSize.A4);
                PdfWriter.GetInstance(document, stream);
                document.Open();

                PdfPTable table = new PdfPTable(3);
                foreach (var reader in list)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(reader.Name.ToString() + " " + reader.Surname.ToString(), font));
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase(reader.Phone));
                    table.AddCell(cell);
                    List<Issue> issues = new List<Issue>();
                    foreach (var supply in reader.Supplys.Where(s => s.Return == false))
                    {
                        foreach(var ent in supply.Entrances)
                        {
                            issues.Add(_context.Issue.First(s => s.Id == ent.IssueId));
                        }
                    }
                    Phrase p = new Phrase();
                    foreach(var iss in issues)
                    {
                        Chunk c = new Chunk(iss.IssueNumber.ToString() + " - " + iss.Publication.Name.ToString(), font);
                        p.Add(c);
                        p.Add(new Chunk("\n"));
                    }
                    cell = new PdfPCell(p);
                    table.AddCell(cell);
                }
                document.Add(table);

                document.Close();
            }

            Process.Start(Application.StartupPath + @"\Debtors.pdf");

        }

        /// <summary>
        /// Автоматизация
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void автоматизацияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int countn = 0;
            int counto = 0;
            OpenFileDialog of = new OpenFileDialog();
            of.Filter = "(*.txt)|*.txt";
            if (of.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (StreamReader sr = new StreamReader(of.FileName, Encoding.Default))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            string[] line2 = line.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
                            string name = line2[0].Split(new char[] { '"' })[1];
                            int[] intMatch = line2[1].Where(Char.IsDigit).Select(x => int.Parse(x.ToString())).ToArray();
                            int number = intMatch[0];
                            intMatch = line2[2].Where(Char.IsDigit).Select(x => int.Parse(x.ToString())).ToArray();
                            int quantity = intMatch[0];

                            if (_context.Publication.Count(s => s.Name.ToUpper() == name.ToUpper()) > 0)
                            {
                                Issue iss = new Issue();
                                iss.Date = DateTime.Now;
                                iss.ISSN = _context.Publication.First(s => s.Name.ToUpper() == name.ToUpper()).ISSN;
                                iss.IssueNumber = number;
                                if (_context.Issue.Count(s => s.ISSN == iss.ISSN && s.IssueNumber == iss.IssueNumber) > 0)
                                {
                                    _context.Issue.First(s => s.ISSN == iss.ISSN && s.IssueNumber == iss.IssueNumber).Quantity += quantity;
                                    counto++;
                                }
                                else
                                {
                                    iss.Period = _context.Issue.Where(s => s.ISSN == iss.ISSN).OrderByDescending(s1 => s1.Date).ToList()[0].Period;
                                    iss.Printing = _context.Issue.Where(s => s.ISSN == iss.ISSN).OrderByDescending(s1 => s1.Date).ToList()[0].Printing;
                                    iss.Quantity = quantity;
                                    _context.Issue.Add(iss);
                                    countn++;
                                }
                            }
                        }

                        _context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {

                }
                MessageBox.Show(String.Format("Добавлено новых выпусков: {0}\nУвеличено кол-во у существующих: {1}", countn, counto), "Информация", MessageBoxButtons.OK);
                this.issueTableAdapter.Fill(
                    this.issueDataSet.Issue);
            }
        }
    }
}
