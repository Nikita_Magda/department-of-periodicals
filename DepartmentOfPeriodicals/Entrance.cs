//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DepartmentOfPeriodicals
{
    using System;
    using System.Collections.Generic;
    
    public partial class Entrance
    {
        public int Id { get; set; }
        public int IssueId { get; set; }
        public int SupplyId { get; set; }
    
        public virtual Issue Issue { get; set; }
        public virtual Supply Supply { get; set; }
    }
}
