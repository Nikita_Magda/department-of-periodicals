﻿namespace DepartmentOfPeriodicals
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.правкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.читателяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.издательствоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.изданиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выпускToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выдачуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.редактироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.запросыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.должникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отсутствующиеВыпускиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.свежееПоступлениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.произвольныйЗапросToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.должникиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewReader = new System.Windows.Forms.DataGridView();
            this.readerNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.surnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phoneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dOBDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.выдатьПериодикуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьВВыдачуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.распечататьЧитательскийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.изменитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.закрытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.readerDataSet = new DepartmentOfPeriodicals.ReaderDataSet();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusType = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageReader = new System.Windows.Forms.TabPage();
            this.numericUpDownReaderNumber = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxReaderSurname = new System.Windows.Forms.TextBox();
            this.textBoxReaderName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonReaderReset = new System.Windows.Forms.Button();
            this.tabPagePublishingHouse = new System.Windows.Forms.TabPage();
            this.dataGridViewPublishingHouse = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phoneDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.publishingHouseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.publishingHouseDataSet = new DepartmentOfPeriodicals.PublishingHouseDataSet();
            this.tabPagePublication = new System.Windows.Forms.TabPage();
            this.buttonPublicationSearch = new System.Windows.Forms.Button();
            this.buttonPubicationReset = new System.Windows.Forms.Button();
            this.numericUpDownLess = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMore = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxSubject = new System.Windows.Forms.ComboBox();
            this.publicationBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.subjectsDataSet = new DepartmentOfPeriodicals.SubjectsDataSet();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.publicationBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.typeDataSet = new DepartmentOfPeriodicals.TypeDataSet();
            this.dataGridViewPublication = new System.Windows.Forms.DataGridView();
            this.iSSNDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.periodicityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subjectsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.publishingHouseIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expr1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.publicationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.publicationDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.publicationDataSet = new DepartmentOfPeriodicals.PublicationDataSet();
            this.tabPageIssue = new System.Windows.Forms.TabPage();
            this.buttonIssueSearch = new System.Windows.Forms.Button();
            this.buttonIssueClear = new System.Windows.Forms.Button();
            this.numericUpDownIssueNumber = new System.Windows.Forms.NumericUpDown();
            this.textBoxIssueName = new System.Windows.Forms.TextBox();
            this.numericUpDownIssueISSN = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridViewIssue = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.issueNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.printingDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iSSNDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.periodDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.issueBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.issueDataSet = new DepartmentOfPeriodicals.IssueDataSet();
            this.tabPageSupply = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.buttonSupplySearch = new System.Windows.Forms.Button();
            this.buttonSupplyClear = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.readerNumberDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.supplyDataSet = new DepartmentOfPeriodicals.SupplyDataSet();
            this.readerTableAdapter = new DepartmentOfPeriodicals.ReaderDataSetTableAdapters.ReaderTableAdapter();
            this.publishingHouseTableAdapter = new DepartmentOfPeriodicals.PublishingHouseDataSetTableAdapters.PublishingHouseTableAdapter();
            this.publicationTableAdapter = new DepartmentOfPeriodicals.PublicationDataSetTableAdapters.PublicationTableAdapter();
            this.issueTableAdapter = new DepartmentOfPeriodicals.IssueDataSetTableAdapters.IssueTableAdapter();
            this.publicationTableAdapter1 = new DepartmentOfPeriodicals.TypeDataSetTableAdapters.PublicationTableAdapter();
            this.publicationTableAdapter2 = new DepartmentOfPeriodicals.SubjectsDataSetTableAdapters.PublicationTableAdapter();
            this.supplyTableAdapter = new DepartmentOfPeriodicals.SupplyDataSetTableAdapters.SupplyTableAdapter();
            this.автоматизацияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReader)).BeginInit();
            this.contextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.readerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.readerDataSet)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPageReader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownReaderNumber)).BeginInit();
            this.tabPagePublishingHouse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPublishingHouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publishingHouseBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publishingHouseDataSet)).BeginInit();
            this.tabPagePublication.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subjectsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.typeDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPublication)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationDataSet)).BeginInit();
            this.tabPageIssue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIssueNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIssueISSN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIssue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.issueBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.issueDataSet)).BeginInit();
            this.tabPageSupply.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.supplyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.supplyDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.правкаToolStripMenuItem,
            this.запросыToolStripMenuItem,
            this.отчетыToolStripMenuItem,
            this.автоматизацияToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(683, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // правкаToolStripMenuItem
            // 
            this.правкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьToolStripMenuItem,
            this.редактироватьToolStripMenuItem,
            this.удалитьToolStripMenuItem});
            this.правкаToolStripMenuItem.Name = "правкаToolStripMenuItem";
            this.правкаToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.правкаToolStripMenuItem.Text = "Правка";
            // 
            // добавитьToolStripMenuItem
            // 
            this.добавитьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.читателяToolStripMenuItem,
            this.издательствоToolStripMenuItem,
            this.изданиеToolStripMenuItem,
            this.выпускToolStripMenuItem,
            this.выдачуToolStripMenuItem});
            this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.добавитьToolStripMenuItem.Text = "Добавить";
            // 
            // читателяToolStripMenuItem
            // 
            this.читателяToolStripMenuItem.Name = "читателяToolStripMenuItem";
            this.читателяToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.читателяToolStripMenuItem.Text = "Читателя";
            this.читателяToolStripMenuItem.Click += new System.EventHandler(this.читателяToolStripMenuItem_Click);
            // 
            // издательствоToolStripMenuItem
            // 
            this.издательствоToolStripMenuItem.Name = "издательствоToolStripMenuItem";
            this.издательствоToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.издательствоToolStripMenuItem.Text = "Издательство";
            this.издательствоToolStripMenuItem.Click += new System.EventHandler(this.издательствоToolStripMenuItem_Click);
            // 
            // изданиеToolStripMenuItem
            // 
            this.изданиеToolStripMenuItem.Name = "изданиеToolStripMenuItem";
            this.изданиеToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.изданиеToolStripMenuItem.Text = "Издание";
            this.изданиеToolStripMenuItem.Click += new System.EventHandler(this.изданиеToolStripMenuItem_Click);
            // 
            // выпускToolStripMenuItem
            // 
            this.выпускToolStripMenuItem.Name = "выпускToolStripMenuItem";
            this.выпускToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.выпускToolStripMenuItem.Text = "Выпуск";
            this.выпускToolStripMenuItem.Click += new System.EventHandler(this.выпускToolStripMenuItem_Click);
            // 
            // выдачуToolStripMenuItem
            // 
            this.выдачуToolStripMenuItem.Name = "выдачуToolStripMenuItem";
            this.выдачуToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.выдачуToolStripMenuItem.Text = "Выдачу";
            // 
            // редактироватьToolStripMenuItem
            // 
            this.редактироватьToolStripMenuItem.Enabled = false;
            this.редактироватьToolStripMenuItem.Name = "редактироватьToolStripMenuItem";
            this.редактироватьToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.редактироватьToolStripMenuItem.Text = "Редактировать";
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Enabled = false;
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            // 
            // запросыToolStripMenuItem
            // 
            this.запросыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.должникиToolStripMenuItem,
            this.отсутствующиеВыпускиToolStripMenuItem,
            this.свежееПоступлениеToolStripMenuItem,
            this.toolStripMenuItem1,
            this.произвольныйЗапросToolStripMenuItem});
            this.запросыToolStripMenuItem.Name = "запросыToolStripMenuItem";
            this.запросыToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.запросыToolStripMenuItem.Text = "Запросы";
            // 
            // должникиToolStripMenuItem
            // 
            this.должникиToolStripMenuItem.Name = "должникиToolStripMenuItem";
            this.должникиToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.должникиToolStripMenuItem.Text = "Должники";
            this.должникиToolStripMenuItem.Click += new System.EventHandler(this.должникиToolStripMenuItem_Click);
            // 
            // отсутствующиеВыпускиToolStripMenuItem
            // 
            this.отсутствующиеВыпускиToolStripMenuItem.Name = "отсутствующиеВыпускиToolStripMenuItem";
            this.отсутствующиеВыпускиToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.отсутствующиеВыпускиToolStripMenuItem.Text = "Отсутствующие выпуски";
            this.отсутствующиеВыпускиToolStripMenuItem.Click += new System.EventHandler(this.отсутствующиеВыпускиToolStripMenuItem_Click);
            // 
            // свежееПоступлениеToolStripMenuItem
            // 
            this.свежееПоступлениеToolStripMenuItem.Name = "свежееПоступлениеToolStripMenuItem";
            this.свежееПоступлениеToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.свежееПоступлениеToolStripMenuItem.Text = "Свежее поступление";
            this.свежееПоступлениеToolStripMenuItem.Click += new System.EventHandler(this.свежееПоступлениеToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(209, 6);
            // 
            // произвольныйЗапросToolStripMenuItem
            // 
            this.произвольныйЗапросToolStripMenuItem.Name = "произвольныйЗапросToolStripMenuItem";
            this.произвольныйЗапросToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.произвольныйЗапросToolStripMenuItem.Text = "Произвольный запрос";
            this.произвольныйЗапросToolStripMenuItem.Click += new System.EventHandler(this.произвольныйЗапросToolStripMenuItem_Click);
            // 
            // отчетыToolStripMenuItem
            // 
            this.отчетыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.должникиToolStripMenuItem1});
            this.отчетыToolStripMenuItem.Name = "отчетыToolStripMenuItem";
            this.отчетыToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.отчетыToolStripMenuItem.Text = "Отчеты";
            // 
            // должникиToolStripMenuItem1
            // 
            this.должникиToolStripMenuItem1.Name = "должникиToolStripMenuItem1";
            this.должникиToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.должникиToolStripMenuItem1.Text = "Должники";
            this.должникиToolStripMenuItem1.Click += new System.EventHandler(this.должникиToolStripMenuItem1_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // dataGridViewReader
            // 
            this.dataGridViewReader.AllowUserToAddRows = false;
            this.dataGridViewReader.AllowUserToDeleteRows = false;
            this.dataGridViewReader.AllowUserToResizeRows = false;
            this.dataGridViewReader.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewReader.AutoGenerateColumns = false;
            this.dataGridViewReader.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewReader.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dataGridViewReader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReader.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.readerNumberDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.surnameDataGridViewTextBoxColumn,
            this.phoneDataGridViewTextBoxColumn,
            this.dOBDataGridViewTextBoxColumn,
            this.addressDataGridViewTextBoxColumn,
            this.finesDataGridViewTextBoxColumn});
            this.dataGridViewReader.ContextMenuStrip = this.contextMenuStrip;
            this.dataGridViewReader.DataSource = this.readerBindingSource;
            this.dataGridViewReader.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewReader.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.dataGridViewReader.MultiSelect = false;
            this.dataGridViewReader.Name = "dataGridViewReader";
            this.dataGridViewReader.ReadOnly = true;
            this.dataGridViewReader.RowHeadersVisible = false;
            this.dataGridViewReader.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewReader.Size = new System.Drawing.Size(669, 324);
            this.dataGridViewReader.TabIndex = 1;
            this.dataGridViewReader.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridViewReader_MouseDown);
            // 
            // readerNumberDataGridViewTextBoxColumn
            // 
            this.readerNumberDataGridViewTextBoxColumn.DataPropertyName = "ReaderNumber";
            this.readerNumberDataGridViewTextBoxColumn.HeaderText = "Номер читательского";
            this.readerNumberDataGridViewTextBoxColumn.Name = "readerNumberDataGridViewTextBoxColumn";
            this.readerNumberDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Имя";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // surnameDataGridViewTextBoxColumn
            // 
            this.surnameDataGridViewTextBoxColumn.DataPropertyName = "Surname";
            this.surnameDataGridViewTextBoxColumn.HeaderText = "Фамилия";
            this.surnameDataGridViewTextBoxColumn.Name = "surnameDataGridViewTextBoxColumn";
            this.surnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // phoneDataGridViewTextBoxColumn
            // 
            this.phoneDataGridViewTextBoxColumn.DataPropertyName = "Phone";
            this.phoneDataGridViewTextBoxColumn.HeaderText = "Телефон";
            this.phoneDataGridViewTextBoxColumn.Name = "phoneDataGridViewTextBoxColumn";
            this.phoneDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dOBDataGridViewTextBoxColumn
            // 
            this.dOBDataGridViewTextBoxColumn.DataPropertyName = "DOB";
            this.dOBDataGridViewTextBoxColumn.HeaderText = "Дата рождения";
            this.dOBDataGridViewTextBoxColumn.Name = "dOBDataGridViewTextBoxColumn";
            this.dOBDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // addressDataGridViewTextBoxColumn
            // 
            this.addressDataGridViewTextBoxColumn.DataPropertyName = "Address";
            this.addressDataGridViewTextBoxColumn.HeaderText = "Адрес";
            this.addressDataGridViewTextBoxColumn.Name = "addressDataGridViewTextBoxColumn";
            this.addressDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // finesDataGridViewTextBoxColumn
            // 
            this.finesDataGridViewTextBoxColumn.DataPropertyName = "Fines";
            this.finesDataGridViewTextBoxColumn.HeaderText = "Штрафы";
            this.finesDataGridViewTextBoxColumn.Name = "finesDataGridViewTextBoxColumn";
            this.finesDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выдатьПериодикуToolStripMenuItem,
            this.добавитьВВыдачуToolStripMenuItem,
            this.распечататьЧитательскийToolStripMenuItem,
            this.изменитьToolStripMenuItem,
            this.удалитьToolStripMenuItem1,
            this.закрытьToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(220, 136);
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_Opening);
            // 
            // выдатьПериодикуToolStripMenuItem
            // 
            this.выдатьПериодикуToolStripMenuItem.Name = "выдатьПериодикуToolStripMenuItem";
            this.выдатьПериодикуToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.выдатьПериодикуToolStripMenuItem.Text = "Выдать периодику";
            this.выдатьПериодикуToolStripMenuItem.Visible = false;
            this.выдатьПериодикуToolStripMenuItem.Click += new System.EventHandler(this.выдатьПериодикуToolStripMenuItem_Click);
            // 
            // добавитьВВыдачуToolStripMenuItem
            // 
            this.добавитьВВыдачуToolStripMenuItem.Name = "добавитьВВыдачуToolStripMenuItem";
            this.добавитьВВыдачуToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.добавитьВВыдачуToolStripMenuItem.Text = "Добавить в выдачу";
            this.добавитьВВыдачуToolStripMenuItem.Click += new System.EventHandler(this.добавитьВВыдачуToolStripMenuItem_Click);
            // 
            // распечататьЧитательскийToolStripMenuItem
            // 
            this.распечататьЧитательскийToolStripMenuItem.Name = "распечататьЧитательскийToolStripMenuItem";
            this.распечататьЧитательскийToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.распечататьЧитательскийToolStripMenuItem.Text = "Распечатать читательский";
            this.распечататьЧитательскийToolStripMenuItem.Visible = false;
            this.распечататьЧитательскийToolStripMenuItem.Click += new System.EventHandler(this.распечататьЧитательскийToolStripMenuItem_Click);
            // 
            // изменитьToolStripMenuItem
            // 
            this.изменитьToolStripMenuItem.Name = "изменитьToolStripMenuItem";
            this.изменитьToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.изменитьToolStripMenuItem.Text = "Изменить";
            this.изменитьToolStripMenuItem.Click += new System.EventHandler(this.изменитьToolStripMenuItem_Click);
            // 
            // удалитьToolStripMenuItem1
            // 
            this.удалитьToolStripMenuItem1.Name = "удалитьToolStripMenuItem1";
            this.удалитьToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.удалитьToolStripMenuItem1.Text = "Удалить";
            this.удалитьToolStripMenuItem1.Click += new System.EventHandler(this.удалитьToolStripMenuItem1_Click);
            // 
            // закрытьToolStripMenuItem
            // 
            this.закрытьToolStripMenuItem.Name = "закрытьToolStripMenuItem";
            this.закрытьToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.закрытьToolStripMenuItem.Text = "Закрыть";
            this.закрытьToolStripMenuItem.Visible = false;
            this.закрытьToolStripMenuItem.Click += new System.EventHandler(this.закрытьToolStripMenuItem_Click);
            // 
            // readerBindingSource
            // 
            this.readerBindingSource.DataMember = "Reader";
            this.readerBindingSource.DataSource = this.readerDataSet;
            // 
            // readerDataSet
            // 
            this.readerDataSet.DataSetName = "ReaderDataSet";
            this.readerDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusType});
            this.statusStrip.Location = new System.Drawing.Point(0, 419);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(683, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusType
            // 
            this.toolStripStatusType.Name = "toolStripStatusType";
            this.toolStripStatusType.Size = new System.Drawing.Size(110, 17);
            this.toolStripStatusType.Text = "toolStripStatusType";
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPageReader);
            this.tabControl.Controls.Add(this.tabPagePublishingHouse);
            this.tabControl.Controls.Add(this.tabPagePublication);
            this.tabControl.Controls.Add(this.tabPageIssue);
            this.tabControl.Controls.Add(this.tabPageSupply);
            this.tabControl.Location = new System.Drawing.Point(0, 27);
            this.tabControl.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(683, 392);
            this.tabControl.TabIndex = 3;
            // 
            // tabPageReader
            // 
            this.tabPageReader.Controls.Add(this.numericUpDownReaderNumber);
            this.tabPageReader.Controls.Add(this.label3);
            this.tabPageReader.Controls.Add(this.label2);
            this.tabPageReader.Controls.Add(this.textBoxReaderSurname);
            this.tabPageReader.Controls.Add(this.textBoxReaderName);
            this.tabPageReader.Controls.Add(this.label1);
            this.tabPageReader.Controls.Add(this.buttonReaderReset);
            this.tabPageReader.Controls.Add(this.dataGridViewReader);
            this.tabPageReader.Location = new System.Drawing.Point(4, 22);
            this.tabPageReader.Name = "tabPageReader";
            this.tabPageReader.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageReader.Size = new System.Drawing.Size(675, 366);
            this.tabPageReader.TabIndex = 0;
            this.tabPageReader.Tag = "Reader";
            this.tabPageReader.Text = "Читатели";
            this.tabPageReader.UseVisualStyleBackColor = true;
            // 
            // numericUpDownReaderNumber
            // 
            this.numericUpDownReaderNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownReaderNumber.Location = new System.Drawing.Point(498, 337);
            this.numericUpDownReaderNumber.Maximum = new decimal(new int[] {
            1410065407,
            2,
            0,
            0});
            this.numericUpDownReaderNumber.Name = "numericUpDownReaderNumber";
            this.numericUpDownReaderNumber.Size = new System.Drawing.Size(93, 20);
            this.numericUpDownReaderNumber.TabIndex = 9;
            this.numericUpDownReaderNumber.ValueChanged += new System.EventHandler(this.buttonReaderSearch_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(394, 340);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "№ читательского:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(151, 340);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Фамилия:";
            // 
            // textBoxReaderSurname
            // 
            this.textBoxReaderSurname.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxReaderSurname.Location = new System.Drawing.Point(216, 337);
            this.textBoxReaderSurname.Name = "textBoxReaderSurname";
            this.textBoxReaderSurname.Size = new System.Drawing.Size(100, 20);
            this.textBoxReaderSurname.TabIndex = 6;
            this.textBoxReaderSurname.TextChanged += new System.EventHandler(this.buttonReaderSearch_Click);
            // 
            // textBoxReaderName
            // 
            this.textBoxReaderName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxReaderName.Location = new System.Drawing.Point(45, 337);
            this.textBoxReaderName.Name = "textBoxReaderName";
            this.textBoxReaderName.Size = new System.Drawing.Size(100, 20);
            this.textBoxReaderName.TabIndex = 5;
            this.textBoxReaderName.TextChanged += new System.EventHandler(this.buttonReaderSearch_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 340);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Имя:";
            // 
            // buttonReaderReset
            // 
            this.buttonReaderReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReaderReset.Location = new System.Drawing.Point(597, 335);
            this.buttonReaderReset.Name = "buttonReaderReset";
            this.buttonReaderReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReaderReset.TabIndex = 2;
            this.buttonReaderReset.Text = "Сбросить";
            this.buttonReaderReset.UseVisualStyleBackColor = true;
            this.buttonReaderReset.Click += new System.EventHandler(this.buttonReaderReset_Click);
            // 
            // tabPagePublishingHouse
            // 
            this.tabPagePublishingHouse.Controls.Add(this.dataGridViewPublishingHouse);
            this.tabPagePublishingHouse.Location = new System.Drawing.Point(4, 22);
            this.tabPagePublishingHouse.Name = "tabPagePublishingHouse";
            this.tabPagePublishingHouse.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePublishingHouse.Size = new System.Drawing.Size(675, 366);
            this.tabPagePublishingHouse.TabIndex = 1;
            this.tabPagePublishingHouse.Tag = "PublishingHouse";
            this.tabPagePublishingHouse.Text = "Издательства";
            this.tabPagePublishingHouse.UseVisualStyleBackColor = true;
            // 
            // dataGridViewPublishingHouse
            // 
            this.dataGridViewPublishingHouse.AllowUserToAddRows = false;
            this.dataGridViewPublishingHouse.AllowUserToDeleteRows = false;
            this.dataGridViewPublishingHouse.AllowUserToResizeRows = false;
            this.dataGridViewPublishingHouse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewPublishingHouse.AutoGenerateColumns = false;
            this.dataGridViewPublishingHouse.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewPublishingHouse.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dataGridViewPublishingHouse.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPublishingHouse.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.nameDataGridViewTextBoxColumn1,
            this.phoneDataGridViewTextBoxColumn1,
            this.addressDataGridViewTextBoxColumn1});
            this.dataGridViewPublishingHouse.ContextMenuStrip = this.contextMenuStrip;
            this.dataGridViewPublishingHouse.DataSource = this.publishingHouseBindingSource;
            this.dataGridViewPublishingHouse.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewPublishingHouse.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.dataGridViewPublishingHouse.MultiSelect = false;
            this.dataGridViewPublishingHouse.Name = "dataGridViewPublishingHouse";
            this.dataGridViewPublishingHouse.ReadOnly = true;
            this.dataGridViewPublishingHouse.RowHeadersVisible = false;
            this.dataGridViewPublishingHouse.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPublishingHouse.Size = new System.Drawing.Size(669, 360);
            this.dataGridViewPublishingHouse.TabIndex = 2;
            this.dataGridViewPublishingHouse.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridViewPublishingHouse_MouseDown);
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn1.HeaderText = "Название";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            this.nameDataGridViewTextBoxColumn1.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn1.Width = 200;
            // 
            // phoneDataGridViewTextBoxColumn1
            // 
            this.phoneDataGridViewTextBoxColumn1.DataPropertyName = "Phone";
            this.phoneDataGridViewTextBoxColumn1.HeaderText = "Телефон";
            this.phoneDataGridViewTextBoxColumn1.Name = "phoneDataGridViewTextBoxColumn1";
            this.phoneDataGridViewTextBoxColumn1.ReadOnly = true;
            this.phoneDataGridViewTextBoxColumn1.Width = 150;
            // 
            // addressDataGridViewTextBoxColumn1
            // 
            this.addressDataGridViewTextBoxColumn1.DataPropertyName = "Address";
            this.addressDataGridViewTextBoxColumn1.HeaderText = "Адрес";
            this.addressDataGridViewTextBoxColumn1.Name = "addressDataGridViewTextBoxColumn1";
            this.addressDataGridViewTextBoxColumn1.ReadOnly = true;
            this.addressDataGridViewTextBoxColumn1.Width = 200;
            // 
            // publishingHouseBindingSource
            // 
            this.publishingHouseBindingSource.DataMember = "PublishingHouse";
            this.publishingHouseBindingSource.DataSource = this.publishingHouseDataSet;
            // 
            // publishingHouseDataSet
            // 
            this.publishingHouseDataSet.DataSetName = "PublishingHouseDataSet";
            this.publishingHouseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPagePublication
            // 
            this.tabPagePublication.Controls.Add(this.buttonPublicationSearch);
            this.tabPagePublication.Controls.Add(this.buttonPubicationReset);
            this.tabPagePublication.Controls.Add(this.numericUpDownLess);
            this.tabPagePublication.Controls.Add(this.numericUpDownMore);
            this.tabPagePublication.Controls.Add(this.label10);
            this.tabPagePublication.Controls.Add(this.label9);
            this.tabPagePublication.Controls.Add(this.comboBoxSubject);
            this.tabPagePublication.Controls.Add(this.label8);
            this.tabPagePublication.Controls.Add(this.label7);
            this.tabPagePublication.Controls.Add(this.comboBoxType);
            this.tabPagePublication.Controls.Add(this.dataGridViewPublication);
            this.tabPagePublication.Location = new System.Drawing.Point(4, 22);
            this.tabPagePublication.Name = "tabPagePublication";
            this.tabPagePublication.Size = new System.Drawing.Size(675, 366);
            this.tabPagePublication.TabIndex = 2;
            this.tabPagePublication.Tag = "Publication";
            this.tabPagePublication.Text = "Издания";
            this.tabPagePublication.UseVisualStyleBackColor = true;
            // 
            // buttonPublicationSearch
            // 
            this.buttonPublicationSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPublicationSearch.Location = new System.Drawing.Point(526, 336);
            this.buttonPublicationSearch.Name = "buttonPublicationSearch";
            this.buttonPublicationSearch.Size = new System.Drawing.Size(66, 23);
            this.buttonPublicationSearch.TabIndex = 12;
            this.buttonPublicationSearch.Text = "Фильтр";
            this.buttonPublicationSearch.UseVisualStyleBackColor = true;
            this.buttonPublicationSearch.Click += new System.EventHandler(this.buttonPublicationSearch_Click);
            // 
            // buttonPubicationReset
            // 
            this.buttonPubicationReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPubicationReset.Location = new System.Drawing.Point(598, 336);
            this.buttonPubicationReset.Name = "buttonPubicationReset";
            this.buttonPubicationReset.Size = new System.Drawing.Size(69, 23);
            this.buttonPubicationReset.TabIndex = 11;
            this.buttonPubicationReset.Text = "Сбросить";
            this.buttonPubicationReset.UseVisualStyleBackColor = true;
            this.buttonPubicationReset.Click += new System.EventHandler(this.buttonPubicationReset_Click);
            // 
            // numericUpDownLess
            // 
            this.numericUpDownLess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownLess.Location = new System.Drawing.Point(476, 338);
            this.numericUpDownLess.Maximum = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this.numericUpDownLess.Name = "numericUpDownLess";
            this.numericUpDownLess.Size = new System.Drawing.Size(43, 20);
            this.numericUpDownLess.TabIndex = 10;
            this.numericUpDownLess.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // numericUpDownMore
            // 
            this.numericUpDownMore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownMore.Location = new System.Drawing.Point(410, 338);
            this.numericUpDownMore.Maximum = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this.numericUpDownMore.Name = "numericUpDownMore";
            this.numericUpDownMore.Size = new System.Drawing.Size(43, 20);
            this.numericUpDownMore.TabIndex = 9;
            this.numericUpDownMore.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(456, 340);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "<=";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(307, 340);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Периодичность: >=";
            // 
            // comboBoxSubject
            // 
            this.comboBoxSubject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboBoxSubject.DataSource = this.publicationBindingSource2;
            this.comboBoxSubject.DisplayMember = "Subjects";
            this.comboBoxSubject.FormattingEnabled = true;
            this.comboBoxSubject.Location = new System.Drawing.Point(205, 337);
            this.comboBoxSubject.Name = "comboBoxSubject";
            this.comboBoxSubject.Size = new System.Drawing.Size(98, 21);
            this.comboBoxSubject.TabIndex = 6;
            this.comboBoxSubject.ValueMember = "Subjects";
            // 
            // publicationBindingSource2
            // 
            this.publicationBindingSource2.DataMember = "Publication";
            this.publicationBindingSource2.DataSource = this.subjectsDataSet;
            // 
            // subjectsDataSet
            // 
            this.subjectsDataSet.DataSetName = "SubjectsDataSet";
            this.subjectsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(140, 340);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Тематика:";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 340);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Тип:";
            // 
            // comboBoxType
            // 
            this.comboBoxType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboBoxType.DataSource = this.publicationBindingSource1;
            this.comboBoxType.DisplayMember = "Type";
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Location = new System.Drawing.Point(41, 337);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(95, 21);
            this.comboBoxType.TabIndex = 3;
            this.comboBoxType.ValueMember = "Type";
            // 
            // publicationBindingSource1
            // 
            this.publicationBindingSource1.DataMember = "Publication";
            this.publicationBindingSource1.DataSource = this.typeDataSet;
            // 
            // typeDataSet
            // 
            this.typeDataSet.DataSetName = "TypeDataSet";
            this.typeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridViewPublication
            // 
            this.dataGridViewPublication.AllowUserToAddRows = false;
            this.dataGridViewPublication.AllowUserToDeleteRows = false;
            this.dataGridViewPublication.AllowUserToResizeRows = false;
            this.dataGridViewPublication.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewPublication.AutoGenerateColumns = false;
            this.dataGridViewPublication.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewPublication.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dataGridViewPublication.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPublication.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iSSNDataGridViewTextBoxColumn1,
            this.nameDataGridViewTextBoxColumn2,
            this.typeDataGridViewTextBoxColumn,
            this.periodicityDataGridViewTextBoxColumn,
            this.subjectsDataGridViewTextBoxColumn,
            this.publishingHouseIdDataGridViewTextBoxColumn,
            this.expr1DataGridViewTextBoxColumn});
            this.dataGridViewPublication.ContextMenuStrip = this.contextMenuStrip;
            this.dataGridViewPublication.DataSource = this.publicationBindingSource;
            this.dataGridViewPublication.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewPublication.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.dataGridViewPublication.MultiSelect = false;
            this.dataGridViewPublication.Name = "dataGridViewPublication";
            this.dataGridViewPublication.ReadOnly = true;
            this.dataGridViewPublication.RowHeadersVisible = false;
            this.dataGridViewPublication.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPublication.Size = new System.Drawing.Size(669, 324);
            this.dataGridViewPublication.TabIndex = 2;
            this.dataGridViewPublication.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridViewPublication_MouseDown);
            // 
            // iSSNDataGridViewTextBoxColumn1
            // 
            this.iSSNDataGridViewTextBoxColumn1.DataPropertyName = "ISSN";
            this.iSSNDataGridViewTextBoxColumn1.HeaderText = "ISSN";
            this.iSSNDataGridViewTextBoxColumn1.Name = "iSSNDataGridViewTextBoxColumn1";
            this.iSSNDataGridViewTextBoxColumn1.ReadOnly = true;
            this.iSSNDataGridViewTextBoxColumn1.Width = 70;
            // 
            // nameDataGridViewTextBoxColumn2
            // 
            this.nameDataGridViewTextBoxColumn2.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn2.HeaderText = "Название";
            this.nameDataGridViewTextBoxColumn2.Name = "nameDataGridViewTextBoxColumn2";
            this.nameDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.typeDataGridViewTextBoxColumn.DataPropertyName = "Type";
            this.typeDataGridViewTextBoxColumn.HeaderText = "Тип";
            this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
            this.typeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // periodicityDataGridViewTextBoxColumn
            // 
            this.periodicityDataGridViewTextBoxColumn.DataPropertyName = "Periodicity";
            this.periodicityDataGridViewTextBoxColumn.HeaderText = "Периодичность";
            this.periodicityDataGridViewTextBoxColumn.Name = "periodicityDataGridViewTextBoxColumn";
            this.periodicityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // subjectsDataGridViewTextBoxColumn
            // 
            this.subjectsDataGridViewTextBoxColumn.DataPropertyName = "Subjects";
            this.subjectsDataGridViewTextBoxColumn.HeaderText = "Тематика";
            this.subjectsDataGridViewTextBoxColumn.Name = "subjectsDataGridViewTextBoxColumn";
            this.subjectsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // publishingHouseIdDataGridViewTextBoxColumn
            // 
            this.publishingHouseIdDataGridViewTextBoxColumn.DataPropertyName = "PublishingHouseId";
            this.publishingHouseIdDataGridViewTextBoxColumn.HeaderText = "Id издательства";
            this.publishingHouseIdDataGridViewTextBoxColumn.Name = "publishingHouseIdDataGridViewTextBoxColumn";
            this.publishingHouseIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // expr1DataGridViewTextBoxColumn
            // 
            this.expr1DataGridViewTextBoxColumn.DataPropertyName = "Expr1";
            this.expr1DataGridViewTextBoxColumn.HeaderText = "Название издательства";
            this.expr1DataGridViewTextBoxColumn.Name = "expr1DataGridViewTextBoxColumn";
            this.expr1DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // publicationBindingSource
            // 
            this.publicationBindingSource.DataMember = "Publication";
            this.publicationBindingSource.DataSource = this.publicationDataSetBindingSource;
            // 
            // publicationDataSetBindingSource
            // 
            this.publicationDataSetBindingSource.DataSource = this.publicationDataSet;
            this.publicationDataSetBindingSource.Position = 0;
            // 
            // publicationDataSet
            // 
            this.publicationDataSet.DataSetName = "PublicationDataSet";
            this.publicationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPageIssue
            // 
            this.tabPageIssue.Controls.Add(this.buttonIssueSearch);
            this.tabPageIssue.Controls.Add(this.buttonIssueClear);
            this.tabPageIssue.Controls.Add(this.numericUpDownIssueNumber);
            this.tabPageIssue.Controls.Add(this.textBoxIssueName);
            this.tabPageIssue.Controls.Add(this.numericUpDownIssueISSN);
            this.tabPageIssue.Controls.Add(this.label6);
            this.tabPageIssue.Controls.Add(this.label5);
            this.tabPageIssue.Controls.Add(this.label4);
            this.tabPageIssue.Controls.Add(this.dataGridViewIssue);
            this.tabPageIssue.Location = new System.Drawing.Point(4, 22);
            this.tabPageIssue.Name = "tabPageIssue";
            this.tabPageIssue.Size = new System.Drawing.Size(675, 366);
            this.tabPageIssue.TabIndex = 3;
            this.tabPageIssue.Tag = "Issue";
            this.tabPageIssue.Text = "Выпуски";
            this.tabPageIssue.UseVisualStyleBackColor = true;
            // 
            // buttonIssueSearch
            // 
            this.buttonIssueSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonIssueSearch.Location = new System.Drawing.Point(511, 335);
            this.buttonIssueSearch.Name = "buttonIssueSearch";
            this.buttonIssueSearch.Size = new System.Drawing.Size(75, 23);
            this.buttonIssueSearch.TabIndex = 10;
            this.buttonIssueSearch.Text = "Найти";
            this.buttonIssueSearch.UseVisualStyleBackColor = true;
            this.buttonIssueSearch.Click += new System.EventHandler(this.buttonIssueSearch_Click);
            // 
            // buttonIssueClear
            // 
            this.buttonIssueClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonIssueClear.Location = new System.Drawing.Point(592, 335);
            this.buttonIssueClear.Name = "buttonIssueClear";
            this.buttonIssueClear.Size = new System.Drawing.Size(75, 23);
            this.buttonIssueClear.TabIndex = 9;
            this.buttonIssueClear.Text = "Сбросить";
            this.buttonIssueClear.UseVisualStyleBackColor = true;
            this.buttonIssueClear.Click += new System.EventHandler(this.buttonIssueClear_Click);
            // 
            // numericUpDownIssueNumber
            // 
            this.numericUpDownIssueNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownIssueNumber.Location = new System.Drawing.Point(406, 337);
            this.numericUpDownIssueNumber.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.numericUpDownIssueNumber.Name = "numericUpDownIssueNumber";
            this.numericUpDownIssueNumber.Size = new System.Drawing.Size(87, 20);
            this.numericUpDownIssueNumber.TabIndex = 8;
            // 
            // textBoxIssueName
            // 
            this.textBoxIssueName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxIssueName.Location = new System.Drawing.Point(204, 337);
            this.textBoxIssueName.Name = "textBoxIssueName";
            this.textBoxIssueName.Size = new System.Drawing.Size(100, 20);
            this.textBoxIssueName.TabIndex = 7;
            // 
            // numericUpDownIssueISSN
            // 
            this.numericUpDownIssueISSN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownIssueISSN.Location = new System.Drawing.Point(49, 337);
            this.numericUpDownIssueISSN.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.numericUpDownIssueISSN.Name = "numericUpDownIssueISSN";
            this.numericUpDownIssueISSN.Size = new System.Drawing.Size(83, 20);
            this.numericUpDownIssueISSN.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(310, 340);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Номер выпуска:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(138, 340);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Название:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 340);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "ISSN:";
            // 
            // dataGridViewIssue
            // 
            this.dataGridViewIssue.AllowUserToAddRows = false;
            this.dataGridViewIssue.AllowUserToDeleteRows = false;
            this.dataGridViewIssue.AllowUserToResizeRows = false;
            this.dataGridViewIssue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewIssue.AutoGenerateColumns = false;
            this.dataGridViewIssue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewIssue.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dataGridViewIssue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewIssue.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nameNumberDataGridViewTextBoxColumn,
            this.issueNumberDataGridViewTextBoxColumn,
            this.dateDataGridViewTextBoxColumn,
            this.printingDataGridViewTextBoxColumn,
            this.iSSNDataGridViewTextBoxColumn,
            this.periodDataGridViewTextBoxColumn,
            this.quantityDataGridViewTextBoxColumn});
            this.dataGridViewIssue.ContextMenuStrip = this.contextMenuStrip;
            this.dataGridViewIssue.DataSource = this.issueBindingSource;
            this.dataGridViewIssue.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewIssue.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.dataGridViewIssue.MultiSelect = false;
            this.dataGridViewIssue.Name = "dataGridViewIssue";
            this.dataGridViewIssue.ReadOnly = true;
            this.dataGridViewIssue.RowHeadersVisible = false;
            this.dataGridViewIssue.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewIssue.Size = new System.Drawing.Size(669, 324);
            this.dataGridViewIssue.TabIndex = 2;
            this.dataGridViewIssue.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridViewIssue_MouseDown);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Width = 50;
            // 
            // nameNumberDataGridViewTextBoxColumn
            // 
            this.nameNumberDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameNumberDataGridViewTextBoxColumn.HeaderText = "Название издания";
            this.nameNumberDataGridViewTextBoxColumn.Name = "nameNumberDataGridViewTextBoxColumn";
            this.nameNumberDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // issueNumberDataGridViewTextBoxColumn
            // 
            this.issueNumberDataGridViewTextBoxColumn.DataPropertyName = "IssueNumber";
            this.issueNumberDataGridViewTextBoxColumn.HeaderText = "Номер выпуска";
            this.issueNumberDataGridViewTextBoxColumn.Name = "issueNumberDataGridViewTextBoxColumn";
            this.issueNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.issueNumberDataGridViewTextBoxColumn.Width = 70;
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Дата";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            this.dateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // printingDataGridViewTextBoxColumn
            // 
            this.printingDataGridViewTextBoxColumn.DataPropertyName = "Printing";
            this.printingDataGridViewTextBoxColumn.HeaderText = "Тираж";
            this.printingDataGridViewTextBoxColumn.Name = "printingDataGridViewTextBoxColumn";
            this.printingDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iSSNDataGridViewTextBoxColumn
            // 
            this.iSSNDataGridViewTextBoxColumn.DataPropertyName = "ISSN";
            this.iSSNDataGridViewTextBoxColumn.HeaderText = "ISSN";
            this.iSSNDataGridViewTextBoxColumn.Name = "iSSNDataGridViewTextBoxColumn";
            this.iSSNDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // periodDataGridViewTextBoxColumn
            // 
            this.periodDataGridViewTextBoxColumn.DataPropertyName = "Period";
            this.periodDataGridViewTextBoxColumn.HeaderText = "Срок выдачи";
            this.periodDataGridViewTextBoxColumn.Name = "periodDataGridViewTextBoxColumn";
            this.periodDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity";
            this.quantityDataGridViewTextBoxColumn.HeaderText = "Кол-во в наличии";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            this.quantityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // issueBindingSource
            // 
            this.issueBindingSource.DataMember = "Issue";
            this.issueBindingSource.DataSource = this.issueDataSet;
            // 
            // issueDataSet
            // 
            this.issueDataSet.DataSetName = "IssueDataSet";
            this.issueDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPageSupply
            // 
            this.tabPageSupply.Controls.Add(this.label11);
            this.tabPageSupply.Controls.Add(this.buttonSupplySearch);
            this.tabPageSupply.Controls.Add(this.buttonSupplyClear);
            this.tabPageSupply.Controls.Add(this.numericUpDown1);
            this.tabPageSupply.Controls.Add(this.dataGridView1);
            this.tabPageSupply.Location = new System.Drawing.Point(4, 22);
            this.tabPageSupply.Name = "tabPageSupply";
            this.tabPageSupply.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSupply.Size = new System.Drawing.Size(675, 366);
            this.tabPageSupply.TabIndex = 4;
            this.tabPageSupply.Text = "Выдачи без возврата";
            this.tabPageSupply.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 340);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Номер читательского:";
            // 
            // buttonSupplySearch
            // 
            this.buttonSupplySearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSupplySearch.Location = new System.Drawing.Point(261, 337);
            this.buttonSupplySearch.Name = "buttonSupplySearch";
            this.buttonSupplySearch.Size = new System.Drawing.Size(75, 22);
            this.buttonSupplySearch.TabIndex = 6;
            this.buttonSupplySearch.Text = "Найти";
            this.buttonSupplySearch.UseVisualStyleBackColor = true;
            this.buttonSupplySearch.Click += new System.EventHandler(this.buttonSupplySearch_Click);
            // 
            // buttonSupplyClear
            // 
            this.buttonSupplyClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSupplyClear.Location = new System.Drawing.Point(594, 335);
            this.buttonSupplyClear.Name = "buttonSupplyClear";
            this.buttonSupplyClear.Size = new System.Drawing.Size(75, 22);
            this.buttonSupplyClear.TabIndex = 5;
            this.buttonSupplyClear.Text = "Сбросить";
            this.buttonSupplyClear.UseVisualStyleBackColor = true;
            this.buttonSupplyClear.Click += new System.EventHandler(this.buttonSupplyClear_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDown1.Location = new System.Drawing.Point(135, 338);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 4;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn2,
            this.readerNumberDataGridViewTextBoxColumn1,
            this.dateDataGridViewTextBoxColumn1});
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip;
            this.dataGridView1.DataSource = this.supplyBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(669, 324);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseDoubleClick);
            // 
            // idDataGridViewTextBoxColumn2
            // 
            this.idDataGridViewTextBoxColumn2.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn2.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn2.Name = "idDataGridViewTextBoxColumn2";
            this.idDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // readerNumberDataGridViewTextBoxColumn1
            // 
            this.readerNumberDataGridViewTextBoxColumn1.DataPropertyName = "ReaderNumber";
            this.readerNumberDataGridViewTextBoxColumn1.HeaderText = "Номер читательского";
            this.readerNumberDataGridViewTextBoxColumn1.Name = "readerNumberDataGridViewTextBoxColumn1";
            this.readerNumberDataGridViewTextBoxColumn1.ReadOnly = true;
            this.readerNumberDataGridViewTextBoxColumn1.Width = 150;
            // 
            // dateDataGridViewTextBoxColumn1
            // 
            this.dateDataGridViewTextBoxColumn1.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn1.HeaderText = "Дата";
            this.dateDataGridViewTextBoxColumn1.Name = "dateDataGridViewTextBoxColumn1";
            this.dateDataGridViewTextBoxColumn1.ReadOnly = true;
            this.dateDataGridViewTextBoxColumn1.Width = 150;
            // 
            // supplyBindingSource
            // 
            this.supplyBindingSource.DataMember = "Supply";
            this.supplyBindingSource.DataSource = this.supplyDataSet;
            // 
            // supplyDataSet
            // 
            this.supplyDataSet.DataSetName = "SupplyDataSet";
            this.supplyDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // readerTableAdapter
            // 
            this.readerTableAdapter.ClearBeforeFill = true;
            // 
            // publishingHouseTableAdapter
            // 
            this.publishingHouseTableAdapter.ClearBeforeFill = true;
            // 
            // publicationTableAdapter
            // 
            this.publicationTableAdapter.ClearBeforeFill = true;
            // 
            // issueTableAdapter
            // 
            this.issueTableAdapter.ClearBeforeFill = true;
            // 
            // publicationTableAdapter1
            // 
            this.publicationTableAdapter1.ClearBeforeFill = true;
            // 
            // publicationTableAdapter2
            // 
            this.publicationTableAdapter2.ClearBeforeFill = true;
            // 
            // supplyTableAdapter
            // 
            this.supplyTableAdapter.ClearBeforeFill = true;
            // 
            // автоматизацияToolStripMenuItem
            // 
            this.автоматизацияToolStripMenuItem.Name = "автоматизацияToolStripMenuItem";
            this.автоматизацияToolStripMenuItem.Size = new System.Drawing.Size(103, 20);
            this.автоматизацияToolStripMenuItem.Text = "Автоматизация";
            this.автоматизацияToolStripMenuItem.Click += new System.EventHandler(this.автоматизацияToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 441);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Отдел периодики";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReader)).EndInit();
            this.contextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.readerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.readerDataSet)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPageReader.ResumeLayout(false);
            this.tabPageReader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownReaderNumber)).EndInit();
            this.tabPagePublishingHouse.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPublishingHouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publishingHouseBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publishingHouseDataSet)).EndInit();
            this.tabPagePublication.ResumeLayout(false);
            this.tabPagePublication.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subjectsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.typeDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPublication)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationDataSet)).EndInit();
            this.tabPageIssue.ResumeLayout(false);
            this.tabPageIssue.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIssueNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIssueISSN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIssue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.issueBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.issueDataSet)).EndInit();
            this.tabPageSupply.ResumeLayout(false);
            this.tabPageSupply.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.supplyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.supplyDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem правкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem запросыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridViewReader;
        private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem редактироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem должникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отсутствующиеВыпускиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem свежееПоступлениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem произвольныйЗапросToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusType;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageReader;
        private System.Windows.Forms.TabPage tabPagePublishingHouse;
        private System.Windows.Forms.TabPage tabPagePublication;
        private System.Windows.Forms.TabPage tabPageIssue;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem изменитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem1;
        private System.Windows.Forms.DataGridView dataGridViewPublishingHouse;
        private System.Windows.Forms.DataGridView dataGridViewPublication;
        private System.Windows.Forms.DataGridView dataGridViewIssue;
        private IssueDataSet issueDataSet;
        private System.Windows.Forms.BindingSource issueBindingSource;
        private IssueDataSetTableAdapters.IssueTableAdapter issueTableAdapter;
        private System.Windows.Forms.BindingSource publicationDataSetBindingSource;
        private PublicationDataSet publicationDataSet;
        private ReaderDataSet readerDataSet;
        private System.Windows.Forms.BindingSource readerBindingSource;
        private ReaderDataSetTableAdapters.ReaderTableAdapter readerTableAdapter;
        private PublishingHouseDataSet publishingHouseDataSet;
        private System.Windows.Forms.BindingSource publishingHouseBindingSource;
        private PublishingHouseDataSetTableAdapters.PublishingHouseTableAdapter publishingHouseTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn readerNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn surnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn phoneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dOBDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn finesDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource publicationBindingSource;
        private PublicationDataSetTableAdapters.PublicationTableAdapter publicationTableAdapter;
        private System.Windows.Forms.NumericUpDown numericUpDownReaderNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxReaderSurname;
        private System.Windows.Forms.TextBox textBoxReaderName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonReaderReset;
        private System.Windows.Forms.Button buttonIssueClear;
        private System.Windows.Forms.NumericUpDown numericUpDownIssueNumber;
        private System.Windows.Forms.TextBox textBoxIssueName;
        private System.Windows.Forms.NumericUpDown numericUpDownIssueISSN;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonIssueSearch;
        private System.Windows.Forms.Button buttonPubicationReset;
        private System.Windows.Forms.NumericUpDown numericUpDownLess;
        private System.Windows.Forms.NumericUpDown numericUpDownMore;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxSubject;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxType;
        private TypeDataSet typeDataSet;
        private System.Windows.Forms.BindingSource publicationBindingSource1;
        private TypeDataSetTableAdapters.PublicationTableAdapter publicationTableAdapter1;
        private SubjectsDataSet subjectsDataSet;
        private System.Windows.Forms.BindingSource publicationBindingSource2;
        private SubjectsDataSetTableAdapters.PublicationTableAdapter publicationTableAdapter2;
        private System.Windows.Forms.Button buttonPublicationSearch;
        private System.Windows.Forms.TabPage tabPageSupply;
        private System.Windows.Forms.ToolStripMenuItem читателяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem издательствоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem изданиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выпускToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выдачуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem распечататьЧитательскийToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn phoneDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iSSNDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodicityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn subjectsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn publishingHouseIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn expr1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn issueNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn printingDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iSSNDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private SupplyDataSet supplyDataSet;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource supplyBindingSource;
        private SupplyDataSetTableAdapters.SupplyTableAdapter supplyTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn readerNumberDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.ToolStripMenuItem добавитьВВыдачуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выдатьПериодикуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem закрытьToolStripMenuItem;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonSupplySearch;
        private System.Windows.Forms.Button buttonSupplyClear;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.ToolStripMenuItem должникиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem автоматизацияToolStripMenuItem;
    }
}

