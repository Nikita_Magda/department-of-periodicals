﻿namespace DepartmentOfPeriodicals.View
{
    partial class FrequentRequests
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.unreturnedDataSet = new DepartmentOfPeriodicals.UnreturnedDataSet();
            this.entranceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.entranceTableAdapter = new DepartmentOfPeriodicals.UnreturnedDataSetTableAdapters.EntranceTableAdapter();
            this.issueIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplyIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.issueNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iSSNDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.returnDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.expr1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expr2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unreturnedDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entranceBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.issueIdDataGridViewTextBoxColumn,
            this.supplyIdDataGridViewTextBoxColumn,
            this.idDataGridViewTextBoxColumn,
            this.issueNumberDataGridViewTextBoxColumn,
            this.iSSNDataGridViewTextBoxColumn,
            this.returnDataGridViewCheckBoxColumn,
            this.expr1DataGridViewTextBoxColumn,
            this.expr2DataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.entranceBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(629, 332);
            this.dataGridView1.TabIndex = 0;
            // 
            // unreturnedDataSet
            // 
            this.unreturnedDataSet.DataSetName = "UnreturnedDataSet";
            this.unreturnedDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // entranceBindingSource
            // 
            this.entranceBindingSource.DataMember = "Entrance";
            this.entranceBindingSource.DataSource = this.unreturnedDataSet;
            // 
            // entranceTableAdapter
            // 
            this.entranceTableAdapter.ClearBeforeFill = true;
            // 
            // issueIdDataGridViewTextBoxColumn
            // 
            this.issueIdDataGridViewTextBoxColumn.DataPropertyName = "IssueId";
            this.issueIdDataGridViewTextBoxColumn.HeaderText = "IssueId";
            this.issueIdDataGridViewTextBoxColumn.Name = "issueIdDataGridViewTextBoxColumn";
            this.issueIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // supplyIdDataGridViewTextBoxColumn
            // 
            this.supplyIdDataGridViewTextBoxColumn.DataPropertyName = "SupplyId";
            this.supplyIdDataGridViewTextBoxColumn.HeaderText = "SupplyId";
            this.supplyIdDataGridViewTextBoxColumn.Name = "supplyIdDataGridViewTextBoxColumn";
            this.supplyIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // issueNumberDataGridViewTextBoxColumn
            // 
            this.issueNumberDataGridViewTextBoxColumn.DataPropertyName = "IssueNumber";
            this.issueNumberDataGridViewTextBoxColumn.HeaderText = "Номер выпуска";
            this.issueNumberDataGridViewTextBoxColumn.Name = "issueNumberDataGridViewTextBoxColumn";
            // 
            // iSSNDataGridViewTextBoxColumn
            // 
            this.iSSNDataGridViewTextBoxColumn.DataPropertyName = "ISSN";
            this.iSSNDataGridViewTextBoxColumn.HeaderText = "ISSN";
            this.iSSNDataGridViewTextBoxColumn.Name = "iSSNDataGridViewTextBoxColumn";
            // 
            // returnDataGridViewCheckBoxColumn
            // 
            this.returnDataGridViewCheckBoxColumn.DataPropertyName = "Return";
            this.returnDataGridViewCheckBoxColumn.HeaderText = "Return";
            this.returnDataGridViewCheckBoxColumn.Name = "returnDataGridViewCheckBoxColumn";
            this.returnDataGridViewCheckBoxColumn.Visible = false;
            // 
            // expr1DataGridViewTextBoxColumn
            // 
            this.expr1DataGridViewTextBoxColumn.DataPropertyName = "Expr1";
            this.expr1DataGridViewTextBoxColumn.HeaderText = "Expr1";
            this.expr1DataGridViewTextBoxColumn.Name = "expr1DataGridViewTextBoxColumn";
            this.expr1DataGridViewTextBoxColumn.ReadOnly = true;
            this.expr1DataGridViewTextBoxColumn.Visible = false;
            // 
            // expr2DataGridViewTextBoxColumn
            // 
            this.expr2DataGridViewTextBoxColumn.DataPropertyName = "Expr2";
            this.expr2DataGridViewTextBoxColumn.HeaderText = "Expr2";
            this.expr2DataGridViewTextBoxColumn.Name = "expr2DataGridViewTextBoxColumn";
            this.expr2DataGridViewTextBoxColumn.Visible = false;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Название издания";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // FrequentRequests
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 356);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FrequentRequests";
            this.Text = "FrequentRequests";
            this.Load += new System.EventHandler(this.FrequentRequests_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unreturnedDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entranceBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private UnreturnedDataSet unreturnedDataSet;
        private System.Windows.Forms.BindingSource entranceBindingSource;
        private UnreturnedDataSetTableAdapters.EntranceTableAdapter entranceTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn issueIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplyIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn issueNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iSSNDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn returnDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn expr1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn expr2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
    }
}