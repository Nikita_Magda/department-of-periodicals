﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DepartmentOfPeriodicals.View
{
    public partial class Debtors : Form
    {
        public Debtors(DepartmentOfPeriodicalsContext _context)
        {
            InitializeComponent();
            this.Text = "Должники";
            BindingList<Reader> list = new BindingList<Reader>();
            foreach (var item in _context.Supply.Where(s => s.Return == false).ToList())
            {
                list.Add(item.Reader);
            }
            dataGridView1.DataSource = list;
        }
    }
}
