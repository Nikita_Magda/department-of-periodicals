﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DepartmentOfPeriodicals.View
{
    public partial class SupplyForm : Form
    {
        public BindingList<Issue> supply;
        public Reader reader;
        public SupplyForm()
        {
            InitializeComponent();
            удалитьToolStripMenuItem.Visible = true;
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = dateTimePicker1.Value.AddDays(3d);
            reader = new Reader();
            supply = new BindingList<Issue>();
            listBox1.DisplayMember = ToString();
            listBox1.ValueMember = "Id";
            button4.Visible = false;
        }

        public SupplyForm(Supply supply, DepartmentOfPeriodicalsContext _context)
        {
            InitializeComponent();
            удалитьToolStripMenuItem.Visible = false;
            dateTimePicker1.Value = supply.Date;
            dateTimePicker2.Value = dateTimePicker1.Value.AddDays(3d);
            textBox1.Text = supply.Reader.Name + " " + supply.Reader.Surname;
            numericUpDown1.Value = supply.ReaderNumber;
            BindingList<Issue> list = new BindingList<Issue>();
            foreach (var item in supply.Entrances)
            {
                list.Add(_context.Issue.First(s => s.Id == item.IssueId));
            }
            listBox1.DisplayMember = ToString();
            listBox1.ValueMember = "Id";
            listBox1.DataSource = list;
            button1.Enabled = false;
            button2.Visible = false;
            button3.Visible = false;
        }

        public void AddElement(Issue issue)
        {
            if (!supply.Contains(issue))
            {
                supply.Add(issue);
                listBox1.DataSource = supply;
                listBox1.Refresh();
            }
            else
            {
                MessageBox.Show("Такой выпуск уже добавлен! Запрещено выдавать на руки более одной копии выпуска!", "Внимание!", MessageBoxButtons.OK);
            }
        }

        public void AddReader(Reader reader)
        {
            if (this.reader.ReaderNumber == 0)
            {
                this.reader = reader;
            }
            else
            {
                MessageBox.Show("Вы уже выбрали читателя! Для изменения удалите текущего!",
                    "Внимание!", MessageBoxButtons.OK);
            }
            numericUpDown1.Value = this.reader.ReaderNumber;
            textBox1.Text = this.reader.Name + " " + this.reader.Surname;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            reader = new Reader();
            numericUpDown1.Value = this.reader.ReaderNumber;
            textBox1.Text = this.reader.Name + " " + this.reader.Surname;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (supply.Count != 0 && reader.ReaderNumber != 0)
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Вы заполнили не все данные!", "Внимание!", MessageBoxButtons.OK);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы уверены, что читатель вернул все выпуски из списка?",
                "Внимание", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            supply.Remove(supply.First(s => s.Id == Int32.Parse(listBox1.SelectedValue.ToString())));
            listBox1.Refresh();
        }
    }
}
