﻿namespace DepartmentOfPeriodicals.View
{
    partial class IssueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label issueNumberLabel;
            System.Windows.Forms.Label dateLabel;
            System.Windows.Forms.Label printingLabel;
            System.Windows.Forms.Label periodLabel;
            System.Windows.Forms.Label quantityLabel;
            System.Windows.Forms.Label nameLabel;
            this.issueNumberNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.dateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.printingNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.periodNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.quantityNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.nameComboBox = new System.Windows.Forms.ComboBox();
            this.publicationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.publicationDataSet = new DepartmentOfPeriodicals.PublicationDataSet();
            this.publicationTableAdapter = new DepartmentOfPeriodicals.PublicationDataSetTableAdapters.PublicationTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.issueDataSet = new DepartmentOfPeriodicals.IssueDataSet();
            this.issueBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.issueTableAdapter = new DepartmentOfPeriodicals.IssueDataSetTableAdapters.IssueTableAdapter();
            this.tableAdapterManager = new DepartmentOfPeriodicals.IssueDataSetTableAdapters.TableAdapterManager();
            issueNumberLabel = new System.Windows.Forms.Label();
            dateLabel = new System.Windows.Forms.Label();
            printingLabel = new System.Windows.Forms.Label();
            periodLabel = new System.Windows.Forms.Label();
            quantityLabel = new System.Windows.Forms.Label();
            nameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.issueNumberNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.issueDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.issueBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // issueNumberLabel
            // 
            issueNumberLabel.AutoSize = true;
            issueNumberLabel.Location = new System.Drawing.Point(12, 9);
            issueNumberLabel.Name = "issueNumberLabel";
            issueNumberLabel.Size = new System.Drawing.Size(90, 13);
            issueNumberLabel.TabIndex = 1;
            issueNumberLabel.Text = "Номер выпуска:";
            // 
            // dateLabel
            // 
            dateLabel.AutoSize = true;
            dateLabel.Location = new System.Drawing.Point(12, 64);
            dateLabel.Name = "dateLabel";
            dateLabel.Size = new System.Drawing.Size(36, 13);
            dateLabel.TabIndex = 3;
            dateLabel.Text = "Дата:";
            // 
            // printingLabel
            // 
            printingLabel.AutoSize = true;
            printingLabel.Location = new System.Drawing.Point(12, 88);
            printingLabel.Name = "printingLabel";
            printingLabel.Size = new System.Drawing.Size(43, 13);
            printingLabel.TabIndex = 5;
            printingLabel.Text = "Тираж:";
            // 
            // periodLabel
            // 
            periodLabel.AutoSize = true;
            periodLabel.Location = new System.Drawing.Point(12, 114);
            periodLabel.Name = "periodLabel";
            periodLabel.Size = new System.Drawing.Size(75, 13);
            periodLabel.TabIndex = 7;
            periodLabel.Text = "Срок выдачи:";
            // 
            // quantityLabel
            // 
            quantityLabel.AutoSize = true;
            quantityLabel.Location = new System.Drawing.Point(12, 140);
            quantityLabel.Name = "quantityLabel";
            quantityLabel.Size = new System.Drawing.Size(122, 13);
            quantityLabel.TabIndex = 9;
            quantityLabel.Text = "Количество в наличии:";
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.Location = new System.Drawing.Point(12, 38);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(105, 13);
            nameLabel.TabIndex = 11;
            nameLabel.Text = "Название издания:";
            // 
            // issueNumberNumericUpDown
            // 
            this.issueNumberNumericUpDown.Location = new System.Drawing.Point(137, 7);
            this.issueNumberNumericUpDown.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.issueNumberNumericUpDown.Name = "issueNumberNumericUpDown";
            this.issueNumberNumericUpDown.Size = new System.Drawing.Size(179, 20);
            this.issueNumberNumericUpDown.TabIndex = 2;
            // 
            // dateDateTimePicker
            // 
            this.dateDateTimePicker.Location = new System.Drawing.Point(137, 60);
            this.dateDateTimePicker.Name = "dateDateTimePicker";
            this.dateDateTimePicker.Size = new System.Drawing.Size(179, 20);
            this.dateDateTimePicker.TabIndex = 4;
            // 
            // printingNumericUpDown
            // 
            this.printingNumericUpDown.Location = new System.Drawing.Point(137, 86);
            this.printingNumericUpDown.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.printingNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.printingNumericUpDown.Name = "printingNumericUpDown";
            this.printingNumericUpDown.Size = new System.Drawing.Size(179, 20);
            this.printingNumericUpDown.TabIndex = 6;
            this.printingNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // periodNumericUpDown
            // 
            this.periodNumericUpDown.Location = new System.Drawing.Point(137, 112);
            this.periodNumericUpDown.Maximum = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this.periodNumericUpDown.Name = "periodNumericUpDown";
            this.periodNumericUpDown.Size = new System.Drawing.Size(179, 20);
            this.periodNumericUpDown.TabIndex = 8;
            this.periodNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // quantityNumericUpDown
            // 
            this.quantityNumericUpDown.Location = new System.Drawing.Point(137, 138);
            this.quantityNumericUpDown.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.quantityNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.quantityNumericUpDown.Name = "quantityNumericUpDown";
            this.quantityNumericUpDown.Size = new System.Drawing.Size(179, 20);
            this.quantityNumericUpDown.TabIndex = 10;
            this.quantityNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nameComboBox
            // 
            this.nameComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.nameComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.nameComboBox.DataSource = this.publicationBindingSource;
            this.nameComboBox.DisplayMember = "Name";
            this.nameComboBox.FormattingEnabled = true;
            this.nameComboBox.Location = new System.Drawing.Point(137, 33);
            this.nameComboBox.Name = "nameComboBox";
            this.nameComboBox.Size = new System.Drawing.Size(179, 21);
            this.nameComboBox.TabIndex = 12;
            this.nameComboBox.ValueMember = "ISSN";
            // 
            // publicationBindingSource
            // 
            this.publicationBindingSource.DataMember = "Publication";
            this.publicationBindingSource.DataSource = this.publicationDataSet;
            // 
            // publicationDataSet
            // 
            this.publicationDataSet.DataSetName = "PublicationDataSet";
            this.publicationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // publicationTableAdapter
            // 
            this.publicationTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 172);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Отмена";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(241, 172);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "ОК";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // issueDataSet
            // 
            this.issueDataSet.DataSetName = "IssueDataSet";
            this.issueDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // issueBindingSource
            // 
            this.issueBindingSource.DataMember = "Issue";
            this.issueBindingSource.DataSource = this.issueDataSet;
            // 
            // issueTableAdapter
            // 
            this.issueTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = DepartmentOfPeriodicals.IssueDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // IssueForm
            // 
            this.AcceptButton = this.button2;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(328, 206);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(issueNumberLabel);
            this.Controls.Add(this.issueNumberNumericUpDown);
            this.Controls.Add(dateLabel);
            this.Controls.Add(this.dateDateTimePicker);
            this.Controls.Add(printingLabel);
            this.Controls.Add(this.printingNumericUpDown);
            this.Controls.Add(periodLabel);
            this.Controls.Add(this.periodNumericUpDown);
            this.Controls.Add(quantityLabel);
            this.Controls.Add(this.quantityNumericUpDown);
            this.Controls.Add(nameLabel);
            this.Controls.Add(this.nameComboBox);
            this.Name = "IssueForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "IssueForm";
            this.Load += new System.EventHandler(this.IssueForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.issueNumberNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.issueDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.issueBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.NumericUpDown issueNumberNumericUpDown;
        private System.Windows.Forms.DateTimePicker dateDateTimePicker;
        private System.Windows.Forms.NumericUpDown printingNumericUpDown;
        private System.Windows.Forms.NumericUpDown periodNumericUpDown;
        private System.Windows.Forms.NumericUpDown quantityNumericUpDown;
        private System.Windows.Forms.ComboBox nameComboBox;
        private PublicationDataSet publicationDataSet;
        private System.Windows.Forms.BindingSource publicationBindingSource;
        private PublicationDataSetTableAdapters.PublicationTableAdapter publicationTableAdapter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private IssueDataSet issueDataSet;
        private System.Windows.Forms.BindingSource issueBindingSource;
        private IssueDataSetTableAdapters.IssueTableAdapter issueTableAdapter;
        private IssueDataSetTableAdapters.TableAdapterManager tableAdapterManager;
    }
}