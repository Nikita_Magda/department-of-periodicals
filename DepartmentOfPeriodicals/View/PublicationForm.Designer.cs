﻿namespace DepartmentOfPeriodicals.View
{
    partial class PublicationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label nameLabel;
            System.Windows.Forms.Label typeLabel;
            System.Windows.Forms.Label periodicityLabel;
            System.Windows.Forms.Label subjectsLabel;
            System.Windows.Forms.Label expr1Label;
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.periodicityNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.subjectsTextBox = new System.Windows.Forms.TextBox();
            this.expr1ComboBox = new System.Windows.Forms.ComboBox();
            this.publishingHouseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.publishingHousesDataSet = new DepartmentOfPeriodicals.PublishingHousesDataSet();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.publishingHouseTableAdapter = new DepartmentOfPeriodicals.PublishingHousesDataSetTableAdapters.PublishingHouseTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            nameLabel = new System.Windows.Forms.Label();
            typeLabel = new System.Windows.Forms.Label();
            periodicityLabel = new System.Windows.Forms.Label();
            subjectsLabel = new System.Windows.Forms.Label();
            expr1Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.periodicityNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publishingHouseBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publishingHousesDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.Location = new System.Drawing.Point(17, 41);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(60, 13);
            nameLabel.TabIndex = 3;
            nameLabel.Text = "Название:";
            // 
            // typeLabel
            // 
            typeLabel.AutoSize = true;
            typeLabel.Location = new System.Drawing.Point(17, 67);
            typeLabel.Name = "typeLabel";
            typeLabel.Size = new System.Drawing.Size(29, 13);
            typeLabel.TabIndex = 5;
            typeLabel.Text = "Тип:";
            // 
            // periodicityLabel
            // 
            periodicityLabel.AutoSize = true;
            periodicityLabel.Location = new System.Drawing.Point(17, 92);
            periodicityLabel.Name = "periodicityLabel";
            periodicityLabel.Size = new System.Drawing.Size(88, 13);
            periodicityLabel.TabIndex = 7;
            periodicityLabel.Text = "Периодичность:";
            // 
            // subjectsLabel
            // 
            subjectsLabel.AutoSize = true;
            subjectsLabel.Location = new System.Drawing.Point(17, 119);
            subjectsLabel.Name = "subjectsLabel";
            subjectsLabel.Size = new System.Drawing.Size(60, 13);
            subjectsLabel.TabIndex = 9;
            subjectsLabel.Text = "Тематика:";
            // 
            // expr1Label
            // 
            expr1Label.AutoSize = true;
            expr1Label.Location = new System.Drawing.Point(17, 145);
            expr1Label.Name = "expr1Label";
            expr1Label.Size = new System.Drawing.Size(82, 13);
            expr1Label.TabIndex = 11;
            expr1Label.Text = "Издательство:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(110, 38);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(121, 20);
            this.nameTextBox.TabIndex = 4;
            // 
            // periodicityNumericUpDown
            // 
            this.periodicityNumericUpDown.Location = new System.Drawing.Point(110, 90);
            this.periodicityNumericUpDown.Maximum = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this.periodicityNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.periodicityNumericUpDown.Name = "periodicityNumericUpDown";
            this.periodicityNumericUpDown.Size = new System.Drawing.Size(121, 20);
            this.periodicityNumericUpDown.TabIndex = 8;
            this.periodicityNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // subjectsTextBox
            // 
            this.subjectsTextBox.Location = new System.Drawing.Point(110, 116);
            this.subjectsTextBox.Name = "subjectsTextBox";
            this.subjectsTextBox.Size = new System.Drawing.Size(121, 20);
            this.subjectsTextBox.TabIndex = 10;
            // 
            // expr1ComboBox
            // 
            this.expr1ComboBox.DataSource = this.publishingHouseBindingSource;
            this.expr1ComboBox.DisplayMember = "Name";
            this.expr1ComboBox.FormattingEnabled = true;
            this.expr1ComboBox.Location = new System.Drawing.Point(110, 142);
            this.expr1ComboBox.Name = "expr1ComboBox";
            this.expr1ComboBox.Size = new System.Drawing.Size(121, 21);
            this.expr1ComboBox.TabIndex = 12;
            this.expr1ComboBox.ValueMember = "Id";
            // 
            // publishingHouseBindingSource
            // 
            this.publishingHouseBindingSource.DataMember = "PublishingHouse";
            this.publishingHouseBindingSource.DataSource = this.publishingHousesDataSet;
            // 
            // publishingHousesDataSet
            // 
            this.publishingHousesDataSet.DataSetName = "PublishingHousesDataSet";
            this.publishingHousesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(17, 181);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Отмена";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(156, 181);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "ОК";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // publishingHouseTableAdapter
            // 
            this.publishingHouseTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "ISSN";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(111, 12);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 16;
            // 
            // typeComboBox
            // 
            this.typeComboBox.FormattingEnabled = true;
            this.typeComboBox.Items.AddRange(new object[] {
            "Журнал",
            "Газета",
            "Ежегодник",
            "Календарь",
            "Библиографический указатель",
            "Справочник"});
            this.typeComboBox.Location = new System.Drawing.Point(110, 63);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(121, 21);
            this.typeComboBox.TabIndex = 17;
            // 
            // PublicationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(243, 218);
            this.Controls.Add(this.typeComboBox);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(nameLabel);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(typeLabel);
            this.Controls.Add(periodicityLabel);
            this.Controls.Add(this.periodicityNumericUpDown);
            this.Controls.Add(subjectsLabel);
            this.Controls.Add(this.subjectsTextBox);
            this.Controls.Add(expr1Label);
            this.Controls.Add(this.expr1ComboBox);
            this.Name = "PublicationForm";
            this.Text = "PublicationForm";
            this.Load += new System.EventHandler(this.PublicationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.periodicityNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publishingHouseBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publishingHousesDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.NumericUpDown periodicityNumericUpDown;
        private System.Windows.Forms.TextBox subjectsTextBox;
        private System.Windows.Forms.ComboBox expr1ComboBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private PublishingHousesDataSet publishingHousesDataSet;
        private System.Windows.Forms.BindingSource publishingHouseBindingSource;
        private PublishingHousesDataSetTableAdapters.PublishingHouseTableAdapter publishingHouseTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.ComboBox typeComboBox;
    }
}