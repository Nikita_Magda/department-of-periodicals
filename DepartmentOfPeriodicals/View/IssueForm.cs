﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DepartmentOfPeriodicals.View
{
    public partial class IssueForm : Form
    {
        public int issueNumber;
        public int issueISSN;
        public DateTime date;
        public int printing;
        public int period;
        public int quantity;

        public IssueForm()
        {
            InitializeComponent();
            this.Text = "Добавление выпуска";
        }

        public IssueForm(Issue issue)
        {
            InitializeComponent();
            this.Text = "Изменение выпуска";
            issueNumberNumericUpDown.Value = issue.IssueNumber;
            nameComboBox.SelectedValue = issue.ISSN;
            dateDateTimePicker.Value = issue.Date;
            printingNumericUpDown.Value = issue.Printing;
            periodNumericUpDown.Value = issue.Period;
            quantityNumericUpDown.Value = issue.Quantity;
        }

        private void IssueForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "publicationDataSet.Publication". При необходимости она может быть перемещена или удалена.
            this.publicationTableAdapter.Fill(this.publicationDataSet.Publication);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (nameComboBox.SelectedValue != null)
            {
                issueNumber = (int)issueNumberNumericUpDown.Value;
                issueISSN = (int)nameComboBox.SelectedValue;
                date = dateDateTimePicker.Value;
                printing = (int)printingNumericUpDown.Value;
                period = (int)periodNumericUpDown.Value;
                quantity = (int)quantityNumericUpDown.Value;
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Error");
            }
        }
    }
}
