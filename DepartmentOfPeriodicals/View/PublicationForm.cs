﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DepartmentOfPeriodicals.View
{
    public partial class PublicationForm : Form
    {
        public int ISSN;
        public string publicationName;
        public string type;
        public int periodicity;
        public string subjects;
        public int publishingHouseId;

        int houseId;

        public PublicationForm()
        {
            InitializeComponent();
        }

        public PublicationForm(Publication publication)
        {
            InitializeComponent();
            numericUpDown1.Enabled = false;
            numericUpDown1.Value = publication.ISSN;
            nameTextBox.Text = publication.Name;
            typeComboBox.SelectedValue = publication.Type;
            periodicityNumericUpDown.Value = publication.Periodicity;
            subjectsTextBox.Text = publication.Subjects;
            houseId = publication.PublishingHouseId;
            typeComboBox.SelectedItem = publication.Type;
        }

        private void PublicationForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "publishingHousesDataSet.PublishingHouse". При необходимости она может быть перемещена или удалена.
            this.publishingHouseTableAdapter.Fill(this.publishingHousesDataSet.PublishingHouse);
            expr1ComboBox.SelectedValue = houseId;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ISSN = (int)numericUpDown1.Value;
            publicationName = nameTextBox.Text;
            type = typeComboBox.SelectedItem.ToString();
            periodicity = (int)periodicityNumericUpDown.Value;
            subjects = subjectsTextBox.Text;
            publishingHouseId = (int)expr1ComboBox.SelectedValue;
            DialogResult = DialogResult.OK;
        }
    }
}
