﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DepartmentOfPeriodicals.View
{
    public partial class ReaderForm : Form
    {
        public string ReaderName;
        public string Surname;
        public string Phone;
        public DateTime DOB;
        public string Address;

        public ReaderForm()
        {
            InitializeComponent();
            this.Text = "Добавление читателя";
        }

        public ReaderForm(Reader reader)
        {
            InitializeComponent();
            textBox1.Text = reader.Name;
            textBox2.Text = reader.Surname;
            textBox3.Text = reader.Phone;
            dateTimePicker1.Value = reader.DOB;
            textBox4.Text = reader.Address;
            this.Text = "Изменение читателя";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ReaderName = textBox1.Text;
            Surname = textBox2.Text;
            Phone = textBox3.Text;
            DOB = dateTimePicker1.Value;
            Address = textBox4.Text;

            DialogResult = DialogResult.OK;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
