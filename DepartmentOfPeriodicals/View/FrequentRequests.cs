﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DepartmentOfPeriodicals.View
{
    public partial class FrequentRequests : Form
    {
        public FrequentRequests()
        {
            InitializeComponent();
            this.Text = "Выпуски, которые не вернули";
            entranceBindingSource.Filter = "Return = 'false'";
        }

        private void FrequentRequests_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "unreturnedDataSet.Entrance". При необходимости она может быть перемещена или удалена.
            this.entranceTableAdapter.Fill(this.unreturnedDataSet.Entrance);

        }
    }
}
