﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DepartmentOfPeriodicals.View
{
    public partial class PublishingHouseForm : Form
    {
        public string PublishingHouseName;
        public string Phone;
        public string Address;

        public PublishingHouseForm()
        {
            InitializeComponent();
            this.Text = "Добавление издательства";
        }

        public PublishingHouseForm(PublishingHouse house)
        {
            InitializeComponent();
            nameTextBox.Text = house.Name;
            phoneTextBox.Text = house.Phone;
            addressTextBox.Text = house.Address;
            this.Text = "Изменение издательства";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PublishingHouseName = nameTextBox.Text;
            Phone = phoneTextBox.Text;
            Address = addressTextBox.Text;
            DialogResult = DialogResult.OK;
        }

        private void phoneTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != 8)
                e.Handled = true;
        }
    }
}
