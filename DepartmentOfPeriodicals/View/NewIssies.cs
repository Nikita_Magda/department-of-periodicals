﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DepartmentOfPeriodicals.View
{
    public partial class NewIssies : Form
    {
        public NewIssies(DepartmentOfPeriodicalsContext _context)
        {
            InitializeComponent();
            this.Text = "Выпуски, которые вышли сегодня";
            BindingList<Issue> list = new BindingList<Issue>();
            foreach (var publication in _context.Publication)
            {
                if (publication.Issues.Count > 0)
                {
                    Issue temp = _context.Issue.Where(s => s.ISSN == publication.ISSN).OrderByDescending(s1 => s1.Date).First();
                    if (temp.Date.AddDays(publication.Periodicity).Date == DateTime.Now.Date)
                    {
                        list.Add(temp);
                    }
                }
            }
            dataGridView1.DataSource = list;
        }
    }
}
